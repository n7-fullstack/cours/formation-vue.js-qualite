# Makina Sapiens - Cours Vue.js

Ce repository contient le support de formation et les TP
concernant la formation "Vue.js",
donnée au sein de Makina Sapiens, mais également l'N7.

Ces cours ont été conçus par Mathieu DARTIGUES en 2017
et mis à jour depuis annuellement.

Dernière date de mise à jour : Octobre 2022.

## Instructions de démarrage

```
npm ci
npm run watch
```

## TPs

Deux TPs sont possibles avec cette formation.

L'un concerne les nonogrammes, l'autre une webapp avec leaflet.

**Nonogramme**


TP : http://n7-nonogramme-vuejs.surge.sh/

Slides : https://n7-vuejs-quality.surge.sh/slides/README.html

**webapp leaflet**

TP : http://mc-formation-vuejs.surge.sh/


## Liens

Attention, les liens des pages HTML pointent sur des `.md`, 
je n'ai pas trouvé l'astuce pour que les liens pointent sur les fichiers `.html`.

Pensez à corriger l'URL de votre navigateur pour transformer le `.md` en `.html`.

## Structure du cours

Les slides sont écrites en Markdown (et utilisent reveal-md)
dans le répertoire `slides`.

Les TPs sont dans le répertoire `tp`.

### Tp Nonogramme / Picross

Le répertoire `tp/picross/vanillajs` contient les corrections
des différentes étapes de la première partie du cours, Vue.js Vanilla.

Le répertoire `tp/vue-cli` contient le projet réalisé
avec la CLI de Vue.js (la v4.x.x).
Pour naviguer entre les étapes de corrections,
voici les correspondances commits / étapes :

* slides CLI / composants
  * étape 1 - 2 : 39e899f3aae6095daccba0adb7ebd538d5b1c1ad
  * étape 3 : bd204a425c66647f602786b5e778e727db5a8bed
  * étape 4 : 92e8a8831a2b1aa581ca4747f58b16a8262018b7
  * étape 5 : 0c1cdcc7963bba8f93d25ddf774611a1243d262e
  * étape 6 : 218cffa77118798767bb1d4dfddcd82e01bffa6c
* slides router
  * étape 7 : 713cc4fff9efbec852ac15c4cb80640080a4d6de
  * étape 8 : 8032b20e8e99be27bc571219fcccd088d19a3699
  * étape 9 : 5b2d78b14359f1ede924455454512e736e7662ff
  * étape 10 : 7479a0a542501068f0d20eab2d4328e805f84617
* slides storybook
  * étape 1 : edd02e102fe47cb0fcaa3b903c595a436cc4300f
  * étape 2 : 654e9a46937e73ea14f8946a0ed527a3cc6f9cce
  * étape 3 : 1ac94dd85c067bcac4686002c5a145624b709b1a
  * étape 4 : 22828f0ff18f6452f7b2ab79ad43d8eeb85c1df7
  * étape 5 : 8650cadd0a210e8e559fb522cd209e713b7569ee
* slides tests (unitaire / intégration)
  * étape 1 : baba5928a1c837bced8f1c5cb7188a20b511812a
  * étape 2 : f447c090aff71de511ab7542a89386417cb3ae62
  * étape 3 : 3a4255efb87280def31e49c353d3fba048a106e1
  * étape 4 : a01caa01a9b7cc00677b421e3b32f9821b58373a
  * étape 5 : e995ad1919eb406f20b79c3135970298fbcd03e0
  * étape 6 : 39104fb5cc9ef48aaf4392362ec57fd0f871eb18
  * étape 7 : 6002652537882ac27b9a45c719190e2b3e93cf40
  * étape 8 : fbdd5250453b018c584e86be6ad6062d35f7c704
* slides intégration continue / déploiement continu
  * CI / CD : dd63c68a9fd1473dce3d8604fbd7c0dcdbfdeb19
  * monitoring : 7d508640a48ee4622b3350aa1cf5629180991f8c

### Tp Webapp Leaflet

Le répertoire `tp/leaflet/vanillajs` contient les corrections
des différentes étapes de la première partie du cours, Vue.js Vanilla.

Le répertoire `tp/leaflet/vue-cli` contient le projet réalisé
avec la CLI de Vue.js (la v4.x.x).
Pour naviguer entre les étapes de corrections,
voici les correspondances commits / étapes :

* slides CLI / composants
  * étape 1 - 2 : 39e899f3aae6095daccba0adb7ebd538d5b1c1ad
  * étape 3 : bd204a425c66647f602786b5e778e727db5a8bed
  * étape 4 : 92e8a8831a2b1aa581ca4747f58b16a8262018b7
  * étape 5 : 0c1cdcc7963bba8f93d25ddf774611a1243d262e
  * étape 6 : 218cffa77118798767bb1d4dfddcd82e01bffa6c
* slides router
  * étape 7 : 713cc4fff9efbec852ac15c4cb80640080a4d6de
  * étape 8 : 8032b20e8e99be27bc571219fcccd088d19a3699
  * étape 9 : 5b2d78b14359f1ede924455454512e736e7662ff
  * étape 10 : 7479a0a542501068f0d20eab2d4328e805f84617
* slides storybook
  * étape 1 : edd02e102fe47cb0fcaa3b903c595a436cc4300f
  * étape 2 : 654e9a46937e73ea14f8946a0ed527a3cc6f9cce
  * étape 3 : 1ac94dd85c067bcac4686002c5a145624b709b1a
  * étape 4 : 22828f0ff18f6452f7b2ab79ad43d8eeb85c1df7
  * étape 5 : 8650cadd0a210e8e559fb522cd209e713b7569ee
* slides tests (unitaire / intégration)
  * étape 1 : baba5928a1c837bced8f1c5cb7188a20b511812a
  * étape 2 : f447c090aff71de511ab7542a89386417cb3ae62
  * étape 3 : 3a4255efb87280def31e49c353d3fba048a106e1
  * étape 4 : a01caa01a9b7cc00677b421e3b32f9821b58373a
  * étape 5 : e995ad1919eb406f20b79c3135970298fbcd03e0
  * étape 6 : 39104fb5cc9ef48aaf4392362ec57fd0f871eb18
  * étape 7 : 6002652537882ac27b9a45c719190e2b3e93cf40
  * étape 8 : fbdd5250453b018c584e86be6ad6062d35f7c704
* slides intégration continue / déploiement continu
  * CI / CD : dd63c68a9fd1473dce3d8604fbd7c0dcdbfdeb19
  * monitoring : 7d508640a48ee4622b3350aa1cf5629180991f8c

## License

This work by Mathieu DARTIGUES is licensed under CC BY-NC-SA 4.0.

To view a copy of this license, visit https://creativecommons.org/licenses/by-nc-sa/4.0
