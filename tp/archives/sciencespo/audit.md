---
title: Formation Vue.js
theme: theme-reveal/makina.css
verticalSeparator: ------
revealOptions:
    transition: 'slide'
---

<!-- .slide: class="title logo" data-background="/images/vuejs-wallpaper.jpeg" -->

# Formation Vue.js

### Audit Sciences Po

---

## Questions

* à quoi sert le `context` ?
  * donné par les entrypoints
  * récupéré par les views + SelectColumn
  * mais jamais utilisé ?
* usage de `async` / `await` ?
* usage des `=>` arrow functions ?

---

<!-- .slide: class="title bg-rocks" -->

# TOP 3

------

# TOP 3 des bonnes pratiques constatées

* filtered values
  * très bon usage du slot
  * composant qui n'altère pas la donnée d'origine
* utilisation du v-model avec le composant SearchField
  * astuces avec https://vuejs.org/v2/guide/forms.html#Modifiers
* détail de la définition des props avec leur valeur par défaut
  * notamment les Array, ou Object qui attendent une fonction

------

# TOP 3 des pratiques à améliorer

* avoir un composant dont le premier noeud a un `v-if` sans `v-else` relève de la responsabilité du parent ?
  * composant `Action`, `Loader`
* utiliser un `data` au lieu d'un `computed`
* nommage des variables (attention collision entre nom de composant et variable de l'instance)
  * composant `Flag`, penser à utiliser des variables qui ne s'entrecoupent pas (icon le composant, icon le svg à afficher)

---

<!-- .slide: class="title bg-lion" -->

# Page Members

------

## Composant Table

------

* `extraClasses` est un tableau. La variable est directement affectable à `class`

```
:class="col.extraClasses"
```

* pour `headerClass`, on peut inscrire directement le code dans le `:class`
* idem pour `sortClass`

cf https://vuejs.org/v2/guide/class-and-style.html

------

* `sortData()` modifie la prop `values` du composant
* une prop ne doit (en général...) jamais être modifiée, principe du "props in, events out"
* dans le code, on peut passer par une shallow copy (ou deep si besoin) de `this.values`

cf https://vuejs.org/v2/guide/components-props.html

------

* est ce qu'il serait pertinent de transformer `sortedValues` en computed property ?
* ainsi, `sortedValues` est automatiquement mis à jour en fonction de ses dépendances
  * sortKey
  * sortOrders
  * values

cf https://vuejs.org/v2/guide/computed.html

------

* casse à respecter sur les noms de méthodes `fill_props` par ex. qui n'est pas en camelCase ?

------

* l'utilisation de la touche espace sur une colonne peut être optimisée, mais ça se discute...
* via l'utilisation des key codes, on peut filtrer l'événement `keyup` directement via un `.space`

cf https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyCode

cf https://vuejs.org/v2/guide/events.html#Key-Modifiers

------

* je n'ai pas compris l'usage de `fillProps`
* mais il me semble que l'usage de `Object.assign` n'est pas nécessaire
* on doit pouvoir affecter directement à `propsA` l'ensemble des données ?

------

* le bouton Edit est un lien, qui doit rediriger vers une autre page de l'application
* en mode SPA, privilégier la remontée d'un event qui sera 'catché' par un parent, si possible un 'smart component' = view

------

## Composant Badge

* l'usage de `data` est fonctionnel, mais lorsque les données évoluent => data ne se met pas à jour

```js
export default {
  name: 'Badge',
  props: {
    value: [String, Array],
  },
  data() {
    const [text, cssClass] = Array.isArray(this.value)
      ? this.value
      : [this.value, 'badge-secondary']
    return { text, cssClass }
  },
};
```

------

* en privilégiant l'usage d'une `computed`, la prop peut évoluer, et le composant également

```js
export default {
  name: 'Badge',
  props: {
    value: [String, Array]
  },
  computed: {
    displayProps () {
      const [text, cssClass] = Array.isArray(this.value)
        ? this.value
        : [this.value, 'badge-secondary']
      return { text, cssClass }
    }
  }
}
```

------

## Composant TableView

* possibilité d'utiliser `async` / `await`
* en mode Promise
```js
  mounted() {
    this.fetchData().then(data => {
      this.values = data;
    }).finally(() => {
      this.isLoading = false
    });
  },
```
* en mode `async` / `await`
```js
  async mounted () {
    this.isLoading = true
    const response = await this.fetchData()
    this.values = response
    this.isLoading = false
  },
```

------

* avec gestion des erreurs

```js
  async mounted () {
    this.isLoading = true
    try {
      const response = await this.fetchData()
      this.values = response.data
    } catch (e) {
      this.error = e
    }
    this.isLoading = false
  },
```
------

# Bilan

* fichier `Table.vue` original : 82 JS + 50 HTML = 132 LoC
* 'optimisé' : 66 JS + 59 HTML = 125 LoC (indentation plus stricte côté HTML / JS => plus de ligne)
* gain paraît faible, mais je pense que c'est plus simple à la maintenance ?

------

# Composants analysés

```
* entrypoints/members.js

* views/generic/TableView.vue

* components/Action.vue
* components/Badge.vue
* components/FilteredValues.vue
* components/Icon.vue
* components/Loader.vue
* components/SearchField.vue
* components/Table.vue
```

---

<!-- .slide: class="title bg-lion" -->

# Page Distribution List

------

* même composant `Table` utilisé

------

* composant Toggle

------

## Composant Toggle

* utilise l'event bus
  * avantages
    * permet de communiquer à différents endroits de l'app
    * fonctionne très bien
  * inconvénients
    * ne respecte pas le "one way data flow"
    * empêche la réutilisation du code dans une autre application
* ma reco
  * privilégier tant que possible, le "props in / events out"
  * si utilisé, pensez à désactiver le handle des events lors du unmount

------

```js
<template>
    <div @click="clickedToggle" v-if="has_children">
        <ArrowRight v-if="!toggled"/>
        <ArrowRight2 v-else/>
    </div>
</template>

<script>
import eventBus from '../eventBus';
import ArrowRight from '@static/icons/arrow-right.svg';
import ArrowRight2 from '@static/icons/arrow-right2.svg';

export default {
  name: 'Toggle',
  components: { ArrowRight, ArrowRight2 },
  props: ['id', 'has_children'],
  created() {
    eventBus.$on('clickedToggle', (id) => {
      this.toggled = !this.toggled && this.id === id;
    });
  },
  data() {
    return {
      toggled: false,
    };
  },
  methods: {
    clickedToggle() {
      eventBus.$emit('clickedToggle', this.id);
    },
  },
};
</script>

```

------

```js
<template>
  <div @click="$emit('toggle')">
    <ArrowRight v-if="!expanded"/>
    <ArrowRight2 v-else/>
  </div>
</template>

<script>
import ArrowRight from './arrow-right.svg'
import ArrowRight2 from './arrow-right2.svg'

export default {
  name: 'Toggle',
  components: { ArrowRight, ArrowRight2 },
  props: {
    expanded: {
      type: Boolean,
      default: false
    }
  }
}
</script>
```
------

Problématiques à résoudre :

Comment transférer les events au parent en mode générique ?

Comment gérer l'affichage du composant s'il n'est pas utile ?

Comment gérer l'état de ce composant ?

------

## Composant TableViewDistributions

------

Bilan:

Composants analysés :

* views/generic/TableViewDistributions.vue

* components/Toggle.vue
* components/Boolean.vue

---

# Reste à analyser

Ces composants n'ont pas été analysés en profondeur,
car il semble qu'ils ne sont pas utilisés au sein de l'application

* ButtonModal  > MessagesDisplay > non utilisé
* MessagesDisplay > non utilisé
* Modal > ButtonModal > MessagesDisplay > non utilisé ? (mais méthode show définie sans être utilisée ?)

* ChildRow > non utilisé
* Flag > non utilisé
  * pertinence du `data` au lieu d'un `computed` si l'on souhaite la réactivité
* Progress > non utilisé
  * `v-if` sur le root element
  * déléguer ce `v-if` au parent qui doit avoir la compétence, pas le composant lui-même
* SelectColumn > non utilisé ?
