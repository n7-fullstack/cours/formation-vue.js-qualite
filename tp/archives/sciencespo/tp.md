---
title: Formation Vue.js
theme: theme-reveal/makina.css
verticalSeparator: ------
revealOptions:
    transition: 'slide'
---

<!-- .slide: class="title logo" data-background="/images/vuejs-wallpaper.jpeg" -->

# Formation Vue.js

### TP Sciences Po

---

## Objectifs

Réutilisation du code

* connexion à l'API (asynchrone / axios / v-model)
* gestion de la persistence de l'authent (localStorage / sessionStorage)
* affichage des résultats dans un composant (alarms / v-bind / v-for)
* affichage des résultats dans plusieurs composants (liste / arbo / map)
* centralisation de la donnée (store)

* tests d'un composant complexe
* mise en place d'un test cypress
* centralisation de la donnée
* mise en place du routing
* mise en place de navigation guard, route authentifiée / ou pas

---

# step-1

instancier un projet avec la CLI 3.0

```
Vue CLI v4.0.5
? Please pick a preset: Manually select features
? Check the features needed for your project:
 ◉ Babel
 ◯ TypeScript
 ◯ Progressive Web App (PWA) Support
 ◉ Router
 ◉ Vuex
 ◯ CSS Pre-processors
 ◉ Linter / Formatter
 ◉ Unit Testing
❯◉ E2E Testing
```

------

```
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, Router, Vuex, Linter, Unit, E2E
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a linter / formatter config: Standard
? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i> to invert selection)Lint on save
? Pick a unit testing solution: Jest
? Pick a E2E testing solution: Cypress
? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? (y/N)
```

démarrer le projet via

```
npm run serve
```

ou

```
yarn serve
```

---

# step-2

* étudier architecture répertoire

------

```
|-public/
|-src/
  |-assets/
  |-components/
  |-router/
  |-store/
  |-views/
  |-App.vue
  |-main.js
|-tests/
|-*.config.js
```

------

* étudier l'UI avec

```
vue ui
```

---

# step-3

* ajouter un storybook
* observer la story du composant à tester (HelloWorld)

------

```
vue add storybook

📦  Installing vue-cli-plugin-storybook...

+ vue-cli-plugin-storybook@0.5.2
added 218 packages from 200 contributors and audited 77719 packages in 33.973s
found 63 low severity vulnerabilities
  run `npm audit fix` to fix them, or `npm audit` for details
✔  Successfully installed plugin: vue-cli-plugin-storybook

? What do you want to generate? Initial framework

🚀  Invoking generator for vue-cli-plugin-storybook...
📦  Installing additional dependencies...

added 13 packages from 8 contributors and audited 78607 packages in 24.137s
found 63 low severity vulnerabilities
  run `npm audit fix` to fix them, or `npm audit` for details
⚓  Running completion hooks...

✔  Successfully invoked generator for plugin: vue-cli-plugin-storybook
```

------

Désormais, on peut lancer le storybook via
```
npm run storybook:serve
```

ou

```
yarn storybook:serve
```

---

# step-4

* ajouter le loader spécifique au projet pour les svg

cf https://vue-svg-loader.js.org/#vue-cli

---

# step-5

Travail sur le composant Table, refactorisation du composant

* reprendre le composant tel qu'il est dans le code actuel
* le faire fonctionner avec une story
  * cela implique de récupérer d'autres composants / configurations / styles
* transformer computed property
* utiliser les classes directement

---

# step-6

Création d'une view qui reprend la Table et les données filtrées

---

# step-7

Examiner le router livré avec le projet, qui a deux routes
* chargement statique d'un composant
* chargement dynamique d'un composant

Ajouter une nouvelle route pour afficher la view créée au step-6

Ajouter un router-link pour y accéder

---

# step-8

Transformer cette route en route dynamique

Constater le chargement asynchrone du composant

---

# step-9

Mise en place de navigation guard, pour une route nécessitant l'authent
et l'autre non

Rajouter une route par défaut, si le user est authent ou non (login par exemple)

Ajouter un bouton permettant de revenir à l'accueil, avec manipulation du $router

---

# step-10

Ajouter test unitaire sur composant Table pour vérifier le tri (analyser les computed)

Ajouter test pour imiter l'usage de Tab + Space pour trier

Ajouter snapshot testing sur les stories + Tab + Space

Ajouter couverture de code

---

# step-11

Création d'un store centralisé

Principe de la réactivité, sans VueX

* centraliser la donnée
  * approche VueX (cf formation)
  * approche vue-reactive-store
* utilisation des devtools

---

# step-12

Ajouter le SSR

---

# Bravo !

Vous avez terminé le TP :-)
