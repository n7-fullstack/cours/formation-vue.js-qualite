---
title: Formation Vue.js
theme: theme-reveal/makina.css
verticalSeparator: ------
revealOptions:
    transition: 'slide'
---

<!-- .slide: class="title logo" data-background="/images/vuejs-wallpaper.jpeg" -->

# Formation Vue.js

### Mathieu Dartigues

#### 5 & 6 Novembre 2019 - Sciences Po

---

<!-- .slide: class="programme bg-lion-left" -->

### [Audit](/tps/sciencespo/audit.md)

------

<!-- .slide: class="programme bg-lion-left" -->

### [TP](/tps/sciencespo/tp.md)

