---
title: Formation Vue.js
theme: theme-reveal/makina.css
verticalSeparator: ------
revealOptions:
    transition: 'slide'
---

<!-- .slide: class="title logo" data-background="/images/vuejs-wallpaper.jpeg" -->

# Formation Vue.js

### TP MWS

---

## Objectifs

Affichage d'alarmes sur un plan

* connexion à l'API (asynchrone / axios / v-model)
* gestion de la persistence de l'authent (localStorage / sessionStorage)
* internationalisation
* récupération des glossaires (markers, refPoints, ...)
* affichage des résultats dans un composant (alarms / v-bind / v-for)
* affichage des résultats dans plusieurs composants (liste / arbo / map)
* centralisation de la donnée (store)

---

# step-1

instancier un projet avec la CLI 3.0

```
Vue CLI v3.5.1
┌───────────────────────────┐
│  Update available: 3.8.2  │
└───────────────────────────┘
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, Router, Linter, Unit
? Use history mode for router? (Requires proper server setup for index fallback in production) No
? Pick a linter / formatter config: Airbnb
? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i> to invert selection)Lint on save
? Pick a unit testing solution: Jest
? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? (y/N) N

```

démarrer le projet via `npm run serve`

---

# step-2

* étudier architecture répertoire

------

```
|-public/
|-src/
  |-assets/
  |-components/
  |-views/
  |-App.vue
  |-main.js
  |-router.js
|-*.config.js
```



---

# step-3

* intégrer framework graphique via plugin vue (vuetify, bootstrap, ...)

------

```
vue add vuetify

...

? Choose a preset: Configure (advanced)
? Use a pre-made template? (will replace App.vue and HelloWorld.vue) Yes
? Use custom theme? Yes
? Use custom properties (CSS variables)? Yes
? Select icon font Material Design Icons
? Use fonts as a dependency (for Electron or offline)? No
? Use a-la-carte components? Yes
? Select locale French
```



---

# step-4

* ajouter un storybook
* observer la story du composant à tester (HelloWorld)

------

```
vue add storybook

📦  Installing vue-cli-plugin-storybook...

+ vue-cli-plugin-storybook@0.5.2
added 218 packages from 200 contributors and audited 77719 packages in 33.973s
found 63 low severity vulnerabilities
  run `npm audit fix` to fix them, or `npm audit` for details
✔  Successfully installed plugin: vue-cli-plugin-storybook

? What do you want to generate? Initial framework

🚀  Invoking generator for vue-cli-plugin-storybook...
📦  Installing additional dependencies...

added 13 packages from 8 contributors and audited 78607 packages in 24.137s
found 63 low severity vulnerabilities
  run `npm audit fix` to fix them, or `npm audit` for details
⚓  Running completion hooks...

✔  Successfully invoked generator for plugin: vue-cli-plugin-storybook
```

------

Désormais, on peut lancer le storybook via
```
npm run storybook:serve
```

---

# step-5

* créer composant Login avec le storybook
  * deux champs de formulaire : email / password
    * quelle directive devons nous utiliser
  * bouton de connexion
* connexion à l'API
  * utilisation de axios
  * url `/login/check_login.php` (param `user` / `passwd`)
  * afficher le SESSID récupéré

---

# step-6

* création d'un service api.js pour les appels axios
  * singleton, avec mono instance axios

---

# step-7

* persister le SESSID
  * utilisation du localStorage

---

# step-8

* modification de l'instance axios pour utiliser l'authent
  * surcharge les headers avec le token s'il existe dans les préférences

---

# step-9

* refactorisation
  * création d'un service preferences.js pour le stockage du token
    * méthode setPreference(key, value)

---

# step-10

* ajouter l'internationalisation (vue-i18n, plugin i18n)
* traduire le formulaire de connexion

------

```
? The locale of project localization. fr
? The fallback locale of project localization. en
? The directory where store localization messages of project. It's stored under `src` directory. locales
? Enable locale messages in Single file components ? Yes
```

---

# step-11

* lors de la connexion réussie de l'utilisateur
  * aller sur une nouvelle page 'visualization'
* récupération des glossaires
  * des markers, ref points, polygons, images, management units
  * des alarmes
* obtenir le tableau d'affichage en computed
  * corréler les données entre elles
  * l'afficher dans un tableau

---

# step-12

* ajout d'un composant Listing d'alarmes
  * utilisation du composant v-data-table
* même chose, pour le composant d'arborescence
  * ajout d'un composant Arborescence
    * utilisation du composant v-treeview
    * et de la directive v-slot

------

Code de la fonction récursive pour l'arbo

```

```

---

# step-13

* affichage des données à travers une carte Leaflet
  * fourniture de code de démarrage (repartir du step-13.start)
  * affichage de polygones sur l'image
* gestion du rafraîchissement des données

---

# step-14

* ajouter des tests / stories
  * valider que les appels à l'API se font avec le bon SESSID
  * afficher une story pour AvailablePlan, AlarmList
* intégrer les stories dans des tests unitaires
* ajouter le snapshot testing
* ajouter la couverture de code

---

# step-15

* centraliser la donnée
  * approche VueX (cf formation)
  * approche vue-reactive-store
* utilisation des devtools

---

# step-16

* builder notre projet
  * via un script `npm`
* déployer notre projet
  * avec des outils en ligne comme `surge` par ex.

---

# Bravo !

Vous avez terminé le TP :-)
