import { mount, createLocalVue } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Home from '@/views/Home.vue';

describe('Home.vue', () => {
  it('renders props.msg when passed', () => {
    const localVue = createLocalVue();
    localVue.use(Vuetify);
    const vm = mount(Home, {
      localVue,
      mocks: {
        $t: () => {},
      },
    });
    expect(vm.element).toMatchSnapshot();
  });
});
