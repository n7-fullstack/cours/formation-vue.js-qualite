/* global describe it expect */
import { mount } from '@vue/test-utils';
import Vuetify from 'vuetify';
import Vue from 'vue';
import Map from './Map.vue';

Vue.use(Vuetify);

// const localVue = createLocalVue();
// localVue.use(Vuetify);


describe('Map.spec.js', () => {
  it('is a Vue instance', () => {
    const vm1 = mount(Map, {
      // localVue,
    });
    expect(vm1.isVueInstance()).toBeTruthy();
  });

  // it('has the right data to display', () => {
  //   const wrapper = mount(Map, {
  //     propsData: {
  //       result: 'Hello World',
  //     },
  //     localVue,
  //   });
  //   expect(wrapper.props().result).toBe('Hello World');
  // });

  it('has the expected html structure', () => {
    const vm3 = mount(Map, {
      propsData: {
        result: [{
          display_name: 'Hello World 2',
          class: 'pouet',
        }, {
          display_name: 'Hello World 3',
          class: 'pouet',
        }],
      },
      // localVue,
    });
    expect(vm3.element).toMatchSnapshot();
  });
});
