const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
  mode: 'development',
  devtool: 'source-map',
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  module: {
    rules: [{
      test: /\.vue$/,
      use: [
        'vue-loader',
      ],
    }]
  },
  plugins: [
    new VueLoaderPlugin()
  ],

  devServer: {
    contentBase: './dist',
  },
}
