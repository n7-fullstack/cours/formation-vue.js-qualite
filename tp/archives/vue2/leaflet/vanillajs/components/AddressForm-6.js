const AddressForm = Vue.component('mc-address-form', {
  template: `<form>
    Road : <input v-model="address.road" type="text" /><br/>
    Zip Code : <input v-model="address.zipCode" type="number" /><br/>
    City : <input v-model="address.city" type="text" /><br/>
    Complement : <input v-model="address.complement" type="text" /><br/>
    Country : <input v-model="address.country" type="text" /><br/>
    State : <input v-model="address.state" type="text" /><br/>
  </form>`,
  props: ['address']
});

export default AddressForm;
