import Vue from 'vue'
import App from './App.vue'
import router from './router'
import * as Sentry from '@sentry/browser'
import { Integrations } from '@sentry/tracing'

Vue.config.productionTip = false

Sentry.init({
  Vue,
  dsn: 'https://8d17b0a919fd4caa9592d1807a2e285c@o421199.ingest.sentry.io/5596254',
  autoSessionTracking: true,
  integrations: [
    new Integrations.BrowserTracing()
  ],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
