import { mount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Game from './Game.vue'

const localVue = createLocalVue()
localVue.use(VueRouter)
const router = new VueRouter()

const picrossData = [{
  title: '4 x 4',
  data: { rows: [[1], [2], [2], [1]], columns: [[1], [2], [2], [1]] },
  size: { rows: 4, columns: 4 }
}]

describe('Game.vue', () => {
  beforeEach(() => {
    fetch.resetMocks()
  })
  it('renders without crashing and by mocking picross data', async () => {
    fetch.mockResponseOnce(JSON.stringify(picrossData))
    const wrapper = mount(Game)
    expect(wrapper).toBeDefined()
    await wrapper.vm.$nextTick() // getting fetch response
    await wrapper.vm.$nextTick() // transforming in json
    expect(fetch.mock.calls.length).toEqual(1)
    expect(fetch.mock.calls[0][0]).toEqual('/data/picross.json')
    expect(wrapper.vm.picrossListing).toEqual(picrossData)
  })
  it('match snapshot by mocking picross data', async () => {
    fetch.mockResponseOnce(JSON.stringify(picrossData))
    const wrapper = mount(Game)
    expect(wrapper).toBeDefined()
    await wrapper.vm.$nextTick() // getting fetch response
    await wrapper.vm.$nextTick() // transforming in json
    await wrapper.vm.$nextTick() // waiting vue refresh DOM
    expect(wrapper).toMatchSnapshot()
  })
  it('match snapshot by clicking on the picross', async () => {
    fetch.mockResponseOnce(JSON.stringify(picrossData))
    const wrapper = mount(Game, {
      localVue,
      router
    })
    expect(wrapper).toBeDefined()
    await wrapper.vm.$nextTick() // getting fetch response
    await wrapper.vm.$nextTick() // transforming in json
    await wrapper.vm.$nextTick() // waiting vue refresh DOM
    wrapper.find('ul li').trigger('click')
    await wrapper.vm.$nextTick() // waiting vue refresh DOM
    expect(wrapper).toMatchSnapshot()
  })
  // it('match snapshot by clicking on the picross and waiting 2s to see if timer is trigerred', async () => {
  //   fetch.mockResponseOnce(JSON.stringify(picrossData))
  //   const wrapper = mount(Game, {
  //     localVue,
  //     router
  //   })
  //   expect(wrapper).toBeDefined()
  //   await wrapper.vm.$nextTick() // getting fetch response
  //   await wrapper.vm.$nextTick() // transforming in json
  //   await wrapper.vm.$nextTick() // waiting vue refresh DOM
  //   wrapper.find('ul li').trigger('click')
  //   await wrapper.vm.$nextTick() // waiting vue refresh DOM
  //   await new Promise(resolve => {
  //     setTimeout(resolve, 3000)
  //   })
  //   expect(wrapper).toMatchSnapshot()
  // })
  // it('match snapshot by finishing a picross', async () => {
  //   fetch.mockResponseOnce(JSON.stringify(picrossData))
  //   const wrapper = mount(Game, {
  //     localVue,
  //     router
  //   })
  //   expect(wrapper).toBeDefined()
  //   await wrapper.vm.$nextTick() // getting fetch response
  //   await wrapper.vm.$nextTick() // transforming in json
  //   await wrapper.vm.$nextTick() // waiting vue refresh DOM
  //   wrapper.find('ul li').trigger('click')
  //   await wrapper.vm.$nextTick() // waiting vue refresh DOM
  //   const cellsEnabled = [
  //     { row: 0, col: 0 },
  //     { row: 1, col: 2 },
  //     { row: 1, col: 3 },
  //     { row: 2, col: 1 },
  //     { row: 2, col: 2 },
  //     { row: 3, col: 1 }
  //   ]
  //   cellsEnabled.forEach(cellEnabled => {
  //     wrapper.find(`table tr:nth-child(${cellEnabled.row + 2}) td:nth-child(${cellEnabled.col + 2})`).trigger('click')
  //   })
  //   await wrapper.vm.$nextTick()
  //   expect(wrapper).toMatchSnapshot()
  // })
})
