import { mount } from '@vue/test-utils'
import PicrossPlayer from './PicrossPlayer.vue'

const picrossData = {
  title: '4 x 4',
  data: { rows: [[1], [2], [2], [1]], columns: [[1], [2], [2], [1]] },
  size: { rows: 4, columns: 4 }
}

describe('PicrossPlayer.vue', () => {
  it('renders without crashing', () => {
    const wrapper = mount(PicrossPlayer)
    expect(wrapper).toBeDefined()
  })
  it('renders with a picross', () => {
    const numberRows = 4
    const numberCells = 4
    const cellsValuesExpected = []
    for (let i = 0; i < numberRows; i++) {
      const currentRowCellsValues = new Array(numberCells).fill(false, 0, numberCells)
      cellsValuesExpected.push(currentRowCellsValues)
    }
    const wrapper = mount(PicrossPlayer, {
      propsData: {
        picrossData
      }
    })
    expect(wrapper).toBeDefined()
    expect(wrapper.vm.cellsValues).toBeDefined()
    expect(wrapper.vm.cellsValues).toEqual(cellsValuesExpected)
  })
  it('trigger `victory` if picross is finished', async () => {
    const onVictory = jest.fn()
    const wrapper = mount(PicrossPlayer, {
      propsData: {
        picrossData
      },
      listeners: {
        victory: onVictory
      }
    })
    const cellsValuesExpected = []
    for (let i = 0; i < picrossData.size.rows; i++) {
      const currentRowCellsValues = new Array(picrossData.size.columns).fill(false)
      cellsValuesExpected.push(currentRowCellsValues)
    }
    cellsValuesExpected[0][0] = true
    cellsValuesExpected[1][2] = true
    cellsValuesExpected[1][3] = true
    cellsValuesExpected[2][1] = true
    cellsValuesExpected[2][2] = true
    cellsValuesExpected[3][1] = true

    expect(wrapper).toBeDefined()
    const cellsEnabled = [
      { row: 0, col: 0 },
      { row: 1, col: 2 },
      { row: 1, col: 3 },
      { row: 2, col: 1 },
      { row: 2, col: 2 },
      { row: 3, col: 1 }
    ]
    cellsEnabled.forEach(cellEnabled => {
      wrapper.find(`table tr:nth-child(${cellEnabled.row + 2}) td:nth-child(${cellEnabled.col + 2})`).trigger('click')
    })
    await wrapper.vm.$nextTick()
    expect(onVictory).toBeCalled()
    expect(wrapper.vm.cellsValues).toEqual(cellsValuesExpected)
  })
  it('match snapshot for a 4x4 picross', () => {
    const wrapper = mount(PicrossPlayer, {
      propsData: {
        picrossData
      }
    })
    expect(wrapper).toMatchSnapshot()
  })
  it('match snapshot for a 4x4 picross finished', async () => {
    const wrapper = mount(PicrossPlayer, {
      propsData: {
        picrossData
      }
    })
    const cellsEnabled = [
      { row: 0, col: 0 },
      { row: 1, col: 2 },
      { row: 1, col: 3 },
      { row: 2, col: 1 },
      { row: 2, col: 2 },
      { row: 3, col: 1 }
    ]
    cellsEnabled.forEach(cellEnabled => {
      wrapper.find(`table tr:nth-child(${cellEnabled.row + 2}) td:nth-child(${cellEnabled.col + 2})`).trigger('click')
    })
    await wrapper.vm.$nextTick()
    expect(wrapper).toMatchSnapshot()
  })
})
