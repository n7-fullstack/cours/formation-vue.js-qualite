import { mount } from '@vue/test-utils'
import PicrossEditor from './PicrossEditor.vue'

describe('PicrossEditor.vue', () => {
  it('renders without crashing', () => {
    const wrapper = mount(PicrossEditor)
    expect(wrapper).toBeDefined()
  })
  it('renders with props', () => {
    const numberRows = 10
    const numberCells = 10
    const wrapper = mount(PicrossEditor, {
      propsData: {
        numberRows,
        numberCells
      }
    })
    const cellsValuesExpected = []
    for (let i = 0; i < numberRows; i++) {
      const currentRowCellsValues = new Array(numberCells).fill(false, 0, numberCells)
      cellsValuesExpected.push(currentRowCellsValues)
    }
    expect(wrapper).toBeDefined()
    expect(wrapper.vm.cellsValues).toBeDefined()
    expect(wrapper.vm.cellsValues).toEqual(cellsValuesExpected)
  })
  it('emit the \'update-picross-data\' event', async () => {
    const numberRows = 10
    const numberCells = 10
    const onUpdatePicrossData = jest.fn()
    const wrapper = mount(PicrossEditor, {
      propsData: {
        numberRows,
        numberCells
      },
      listeners: {
        'update-picross-data': onUpdatePicrossData
      }
    })
    const cellsValuesExpected = []
    for (let i = 0; i < numberRows; i++) {
      const currentRowCellsValues = new Array(numberCells).fill(false, 0, numberCells)
      cellsValuesExpected.push(currentRowCellsValues)
    }
    cellsValuesExpected[0][1] = true
    expect(wrapper).toBeDefined()
    /**
     * find a cell and click on it
     * we add 2 for number of rows / cells
     * because we have a row and a column for Player tips
     * and because we have 0 based arrays
    */
    const wrapperCell = wrapper.find('table tr:nth-child(2) td:nth-child(3)')
    expect(wrapperCell).toBeDefined()
    wrapperCell.trigger('click')
    await wrapper.vm.$nextTick()
    expect(onUpdatePicrossData).toBeCalled()
    expect(wrapper.vm.cellsValues).toEqual(cellsValuesExpected)
  })
  it('match snapshot for a 10x10 picross', () => {
    const numberRows = 10
    const numberCells = 10
    const wrapper = mount(PicrossEditor, {
      propsData: {
        numberRows,
        numberCells
      }
    })
    expect(wrapper).toMatchSnapshot()
  })
  it('match image snapshot for a 10x10 picross and two cells enabled', async () => {
    const numberRows = 10
    const numberCells = 10
    const wrapper = mount(PicrossEditor, {
      propsData: {
        numberRows,
        numberCells
      }
    })
    wrapper.find('table tr:nth-child(2) td:nth-child(3)').trigger('click')
    wrapper.find('table tr:nth-child(3) td:nth-child(4)').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper).toMatchSnapshot()
  })
})
