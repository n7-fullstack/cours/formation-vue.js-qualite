import PicrossEditor from './PicrossEditor.vue'
import { action } from '@storybook/addon-actions'

export default {
  title: 'components/PicrossEditor',
  component: PicrossEditor
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PicrossEditor },
  template: '<picross-editor @update-picross-data="onClick" v-bind="$props" />',
  methods: {
    onClick: action('update-picross-data')
  }
})

export const defaultStory = Template.bind({})
defaultStory.storyName = 'default'
defaultStory.args = {
  numberRows: 10,
  numberCells: 10
}
