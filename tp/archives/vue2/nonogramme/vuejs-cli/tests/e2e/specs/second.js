// https://docs.cypress.io/api/introduction/api.html

describe('Deuxième scénario', () => {
  it('Crée un nonogramme de 10 x 10', () => {
    cy.visit('/')
    cy.get('[href="#/editor"]').click()
    // cy.wait(500)
    cy.get('[data-cypress="config"]')
      .children('input:first')
      .type('{backspace}10')
      .parent()
      .children('input:nth-of-type(2)')
      .type('{backspace}10')
    cy.get('[data-cypress="config"] button').click()
    cy.get('#app').toMatchImageSnapshot({
      threshold: 0.001
    })
  })
})
