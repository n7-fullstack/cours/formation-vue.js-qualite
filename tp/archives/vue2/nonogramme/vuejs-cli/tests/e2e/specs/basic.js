// https://docs.cypress.io/api/introduction/api.html

describe('Scénario basique', () => {
  it('Ouvre la page d\'accueil', () => {
    cy.visit('/')
    // cy.get('[href="#/game"]').click()
    cy.wait(500)
    cy.get('#app').toMatchImageSnapshot({
      threshold: 0.001
    })
    cy.get('h1').should('contain', 'Bienvenue sur l\'app nonogramme en Vue.js !')
  })
  it('Génère un nonogramme de 10 x 10', () => {
    cy.visit('/#/editor')
    cy.wait(500)
    // cy.get('input[type="number"]').forEach(input => input.type('{backspace}10'))
    cy.get('[data-cypress="config"]')
      .children('input[type="number"]:first')
      .type('{backspace}10')
      .parent()
      .children('input[type="number"]:nth-of-type(2)')
      .type('{backspace}10')
      .parent()
      .children('button').click()
    cy.get('#app').toMatchImageSnapshot({
      threshold: 0.001
    })
  })
  it('Affiche le picross 4 x 4', () => {
    cy.visit('/#/game')
    cy.get('ul li[data-title="4 x 4"]').click()
    cy.wait(5000)
    cy.get('#app').toMatchImageSnapshot({
      threshold: 0.001
    })
  })
  it('Affiche victoire quand on résoud le picross', () => {
    cy.visit('/#/game')
    cy.get('ul li[data-title="4 x 4"]').click()
    const cellsEnabled = [
      { row: 0, col: 0 },
      { row: 1, col: 2 },
      { row: 1, col: 3 },
      { row: 2, col: 1 },
      { row: 2, col: 2 },
      { row: 3, col: 1 }
    ]
    cellsEnabled.forEach(cellEnabled => {
      cy.get(`table tr:nth-child(${cellEnabled.row + 2}) td:nth-child(${cellEnabled.col + 2})`).click()
    })
    cy.get('body').toMatchImageSnapshot({
      threshold: 0.001
    })
  })
})
