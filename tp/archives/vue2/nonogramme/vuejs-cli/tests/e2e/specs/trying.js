// https://docs.cypress.io/api/introduction/api.html

describe('Tests d\'intégration n7', () => {
  it('Ouvre la page d\'accueil et vérifie l\'image', () => {
    cy.visit('/')
    cy.contains('h1', 'Bienvenue sur l\'app nonogramme en Vue.js !')
    cy.wait(500)
  })
  it('Crée un nonogramme de 10 x 10', () => {
    cy.visit('/')
    cy.get('[href="#/editor"]').click()
    // cy.wait(500)
    cy.get('[data-cypress="config"]')
      .children('input:first')
      .type('{backspace}10')
      .parent()
      .children('input:nth-of-type(2)')
      .type('{backspace}10')
    cy.get('[data-cypress="config"] button').click()
  })
  it('Ouvre un picross', () => {
    cy.visit('/')
    cy.get('[href="#/game"]').click()
    cy.get('ul > li:first').click()
  })
  it('Finis un picross', () => {
    cy.visit('/')
    cy.get('[href="#/game"]').click()
    cy.get('ul > li:first').click()
    const cellsEnabled = [
      { row: 0, col: 0 },
      { row: 1, col: 2 },
      { row: 1, col: 3 },
      { row: 2, col: 1 },
      { row: 2, col: 2 },
      { row: 3, col: 1 }
    ]
    cellsEnabled.forEach(cellEnabled => {
      cy.get(`table tr:nth-child(${cellEnabled.row + 2}) td:nth-child(${cellEnabled.col + 2})`).click()
    })
    cy.get('h1').should('contain', 'Victoire')
  })
})
