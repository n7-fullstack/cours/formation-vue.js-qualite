module.exports = {
  preset: '@vue/cli-plugin-unit-jest',

  testMatch: [
    '**/*.spec.(js|jsx|ts|tsx)'
  ],
  automock: false,
  setupFiles: [
    './tests/unit/setupJest.js'
  ],

  collectCoverageFrom: [
    'src/**/*.{js,ts,vue}',
    '!src/**/*.stories.js',
    '!src/**/*.spec.js',
    '!src/registerServiceWorker.ts'
  ]

}
