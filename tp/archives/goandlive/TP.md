# Formation Vue.js Go And Live

### Objectifs

* connexion à l'API
* gestion de l'authent
* persistence de l'authent
* récupération des glossaires
* formulaire de recherche
* la gestion de l'internationalisation (i18n)
* génération côté serveur (SSR)

# step-1

instancier un projet avec la CLI 3.0

```
Vue CLI v3.0.0
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, PWA, Router, Vuex, CSS Pre-processors, Linter, Unit, E2E
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): SCSS/SASS
? Pick a linter / formatter config: Airbnb
? Pick additional lint features: Lint on save
? Pick a unit testing solution: Jest
? Pick a E2E testing solution: Cypress
? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? (y/N)
```

démarrer le projet via `npm run serve`

# step-2

* intégrer framework graphique via plugin vue

# step-3

* ajouter le SSR
* vérifier que tout fonctionne

# step-4

* ajouter l'internationalisation i18n

# step-5

* ajouter un storybook
* ajouter un composant à tester (HelloWorld)

# step-6

* créer page Login
  * deux champs de formulaire : email / password
  * bouton de connexion
* connexion à l'API
  * utilisation de axios
  * url `https://api.goandlive.com/auth?email=presentationApi@goandlive.com&password=4s5F9vjG`
  * afficher le token récupéré
* création d'un service api.js pour les appels axios
  * singleton, avec mono instance axios
  * surcharge les headers avec le token s'il existe dans les préférences

# step-7

* persister le token
  * utilisation du localStorage
  * rediriger l'utilisateur vers une autre page Settings qui affiche le token
* refactorisation
  * création d'un service preferences.js pour le stockage du token
    * méthode setPreference(key, value)

# step-8

* récupération des glossaires
  * augmenter le service api avec récupération
    * des catégories
    * des villes
    * ...
* ajout d'un composant Listing
* utilisation du composant v-tab
* définition des méthodes à appeler selon le glossaire à afficher

# step-9

* ajouter des tests / stories
* intégrer les stories dans des tests unitaires
* ajouter la couverture de code

# step-10

* création d'un formulaire de recherche
* affichage des résultats de la recherche
* utilisation d'un store VueX pour la recherche
* prise en compte des paramètres dans l'URL pour la recherche côté SSR