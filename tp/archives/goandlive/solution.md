# Formation Vue.js Go And Live

## Solution

# step-1

instancier un projet avec la CLI 3.0

```
Vue CLI v3.0.0
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, Router, Vuex, Linter, Unit
? Pick a linter / formatter config: Standard
? Pick additional lint features: Lint on save
? Pick a unit testing solution: Jest
? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? (y/N)
```

démarrer le projet via `npm run serve`

# step-2

```
vue add vuetify
```

# step-3

```
vue add @uvue/ssr

📦  Installing @uvue/vue-cli-plugin-ssr...


> nodemon@1.18.10 postinstall /home/mdartic/workspace/sapiens/goandlive/node_modules/nodemon
> node bin/postinstall || exit 0

+ @uvue/vue-cli-plugin-ssr@0.1.0-alpha.16
added 86 packages from 91 contributors and audited 52170 packages in 21.327s
found 64 low severity vulnerabilities
  run `npm audit fix` to fix them, or `npm audit` for details
✔  Successfully installed plugin: @uvue/vue-cli-plugin-ssr

? Install UVue plugins? (Press <space> to select, <a> to toggle all, <i> to invert selection)Async Data method, Vuex, Middlewares system, Error handler
? Enable Vuex behaviors Use onHttpRequest() Vuex actions, Use fetch() methods on components
? Install server plugins? (Press <space> to select, <a> to toggle all, <i> to invert selection)GZIP compression, Server error page, Static files serving, Modern build
? Do you want to use Docker to deploy your app? No
```

Configurer le fichier `vue.config.js` :

```
module.exports = {
  transpileDependencies: [/^vuetify/]
}
```

S'assurer que le mode du routeur est bien sur `history`.

# step-4

```
vue add i18n

📦  Installing vue-cli-plugin-i18n...

+ vue-cli-plugin-i18n@0.5.1
added 7 packages from 7 contributors and audited 69322 packages in 17.099s
found 63 low severity vulnerabilities
  run `npm audit fix` to fix them, or `npm audit` for details
✔  Successfully installed plugin: vue-cli-plugin-i18n

? The locale of project localization. en
? The fallback locale of project localization. en
? The directory where store localization messages of project. It's stored under `src` directory. locales
? Enable locale messages in Single file components ? No

🚀  Invoking generator for vue-cli-plugin-i18n...
📦  Installing additional dependencies...

audited 69323 packages in 19.706s
found 63 low severity vulnerabilities
  run `npm audit fix` to fix them, or `npm audit` for details
⚓  Running completion hooks...

✔  Successfully invoked generator for plugin: vue-cli-plugin-i18n
```

# step-5

```
vue add storybook

📦  Installing vue-cli-plugin-storybook...

+ vue-cli-plugin-storybook@0.5.2
added 218 packages from 200 contributors and audited 77719 packages in 33.973s
found 63 low severity vulnerabilities
  run `npm audit fix` to fix them, or `npm audit` for details
✔  Successfully installed plugin: vue-cli-plugin-storybook

? What do you want to generate? Initial framework

🚀  Invoking generator for vue-cli-plugin-storybook...
📦  Installing additional dependencies...

added 13 packages from 8 contributors and audited 78607 packages in 24.137s
found 63 low severity vulnerabilities
  run `npm audit fix` to fix them, or `npm audit` for details
⚓  Running completion hooks...

✔  Successfully invoked generator for plugin: vue-cli-plugin-storybook
```

Désormais, on peut lancer le storybook via
```
npm run storybook:serve
```


# step-6

* créer page Login
  * deux champs de formulaire : email / password
  * bouton de connexion
* connexion à l'API
  * utilisation de axios
  * url `https://api.goandlive.com/auth?email=presentationApi@goandlive.com&password=4s5F9vjG`
  * afficher le token récupéré
* création d'un service api.js pour les appels axios
  * singleton, avec mono instance axios
  * surcharge les headers avec le token s'il existe dans les préférences

# step-7

* persister le token
  * utilisation du localStorage
  * rediriger l'utilisateur vers une autre page Settings qui affiche le token
* refactorisation
  * création d'un service preferences.js pour le stockage du token
    * méthode setPreference(key, value)

# step-8

* récupération des glossaires
  * augmenter le service api avec récupération
    * des catégories
    * des villes
    * ...
* ajout d'un composant Listing
* utilisation du composant v-tab
* définition des méthodes à appeler selon le glossaire à afficher

# step-9

* ajouter des tests / stories
  * écriture de stories pour Listing & HelloWorld
  * écriture de tests pour api
  * les fichiers de tests sont des fichiers *.spec.js
* intégrer les stories dans des tests unitaires
  * utilisation de l'addon storyshot
  * configuration pénible à exécuter pour 
    * intégrer le plugin vuetify
    * spécifier l'ensemble des composants vuetify nécessaires
    * spécifier les composants Vuetify par composant goandlive
    * améliorer les confs de storybook et jest pour accepter Vuetify
* ajouter la couverture de code
  * configuration de jest.config.js

# step-10

* création d'un composant FormSearch
* création de la view Search
* ajout d'un store VueX search, pour dispatcher une recherche
* prise en compte des données côté serveur avec asyncData