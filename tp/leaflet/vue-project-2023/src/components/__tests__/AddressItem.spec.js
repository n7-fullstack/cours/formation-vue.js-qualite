import { mount } from "@vue/test-utils";
import { describe, it, expect, vi } from "vitest";

import AddressItem from "../AddressItem.vue";

describe("AddressItem.vue", () => {
  const address = {
    road: "3 Villeneuve",
    zipCode: "44130",
    city: "Notre dame des Landes",
    complement: "ZAD",
    country: "Loire Atlantique",
  };
  it("crashes if no props are given", () => {
    expect.assertions(1);
    try {
      mount(AddressItem);
    } catch (error) {
      expect(error).toBeDefined();
    }
  });
  it("renders with props", () => {
    expect.assertions(3);
    const wrapper = mount(AddressItem, {
      props: {
        address,
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.text()).toContain("ZAD");
    expect(wrapper.text()).toContain("Notre dame des Landes");
  });

  it("emit the update event when clicking on update button", () => {
    expect.assertions(5);
    const wrapper = mount(AddressItem, {
      props: {
        address,
      },
    });
    const wrapperButtons = wrapper.findAll("button");
    expect(wrapperButtons.length).toBe(2);
    wrapperButtons[0].trigger("click");
    expect(wrapper.emitted()).toHaveProperty("update");
    expect(wrapper.emitted()).not.toHaveProperty("search");
    expect(wrapper.emitted()["update"]).toHaveLength(1);
    wrapperButtons[0].trigger("click");
    expect(wrapper.emitted()["update"]).toHaveLength(2);
  });

  it.todo("emit the search event when clicking on search button");

  it("match the snapshot with a full address", () => {
    expect.assertions(1);
    const wrapper = mount(AddressItem, {
      props: {
        address,
      },
    });
    expect(wrapper.html()).toMatchSnapshot();
  });
});
