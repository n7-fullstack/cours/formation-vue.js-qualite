import { reactive } from "vue";

export function useAddress() {
  const state = reactive({
    addressList: {
      from: {
        label: "Origine",
        road: "11 rue du Marchix",
        zipCode: "44000",
        city: "Nantes",
        complement: "Makina Corpus",
        country: "Pays de la Loire", // ici, nous ne déclarons pas la variable "state"
      },
      to: {
        label: "Destination",
        road: "Château des ducs de Bretagne",
        zipCode: "44000",
        city: "Nantes",
        state: "France",
      },
    },
    addressToEdit: null,
    coordinates: null,
    loading: false,
    currentCoordinate: null,
  });

  async function searchAddress(address) {
    state.loading = true;
    try {
      const response = await fetch(
        "https://api-adresse.data.gouv.fr/search/?q=" +
          address.road +
          " " +
          address.zipCode +
          " " +
          address.city +
          " "
      );
      const json = await response.json();
      json.features.forEach((currentFeature) => {
        currentFeature.geometry.coordinates.reverse();
      });
      state.coordinates = json.features;
      if (json.features.length > 0) state.currentCoordinate = json.features[0];
    } catch (e) {
      // do nothing
      state.error = e;
    }
    state.loading = false;
  }

  return {
    state,
    searchAddress,
  };
}
