export default {
  links: {
    home: "Accueil",
    about: "À propos",
    search: "Recherche",
  },
  views: {
    search: {
      from: "Adresse d'origine",
      to: "Adresse de destination",
    },
  },
  components: {
    Address: {
      buttons: {
        edit: "Modifier",
        search: "Chercher",
      },
    },
  },
};
