export default {
  links: {
    home: "Home",
    about: "About",
    search: "Search",
  },
  views: {
    search: {
      from: "Address from",
      to: "Adress to",
    },
  },
  components: {
    Address: {
      buttons: {
        edit: "Edit",
        search: "Search",
      },
    },
  },
};
