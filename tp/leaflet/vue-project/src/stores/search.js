import { computed, reactive } from "vue";
import { defineStore } from "pinia";

export const useSearchStore = defineStore("search", () => {
  const state = reactive({
    loading: false,
    error: null,
    coordinates: [],
    currentCoordinate: null,
  });
  async function searchAddress(address) {
    state.loading = true;
    try {
      const response = await fetch(
        "https://api-adresse.data.gouv.fr/search/?q=" +
          address.road +
          " " +
          address.zipCode +
          " " +
          address.city +
          " "
      );
      const json = await response.json();
      json.features.forEach((currentFeature) => {
        currentFeature.geometry.coordinates.reverse();
      });
      state.coordinates = json.features;
      if (json.features.length > 0) state.currentCoordinate = json.features[0];
    } catch (e) {
      // do nothing
      state.error = e;
    }
    state.loading = false;
  }

  const coordinatesLength = computed(() => {
    return state.coordinates.length;
  });

  return { state, coordinatesLength, searchAddress };
});
