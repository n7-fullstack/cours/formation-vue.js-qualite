import { createApp } from "vue";
import { createPinia } from "pinia";
import { createI18n } from "vue-i18n";

import fr from "./lang/fr_FR";
import en from "./lang/en_EN";

const i18n = createI18n({
  // something vue-i18n options here ...
  locale: "fr",
  fallbackLocale: "en",
  messages: {
    en: en,
    fr: fr,
  },
});
import App from "./App.vue";
import router from "./router";

import "./assets/main.css";

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(i18n);

app.mount("#app");
