import { mount } from "@vue/test-utils";
import { describe, it, expect } from "vitest";
import AddressItem from "../AddressItem.vue";

const address = {
  label: "My address",
  city: "Nantes",
  zipCode: "44000",
  road: "rue du Marchix",
  state: "France",
};

describe("AddressItem.vue", () => {
  it("renders without crashing", () => {
    const wrapper = mount(AddressItem);
    expect(wrapper).toBeDefined();
    expect(wrapper.html()).toMatchSnapshot();
  });
  it("renders with props", () => {
    const wrapper = mount(AddressItem, {
      propsData: {
        address,
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.text()).toContain("My address");
    expect(wrapper.text()).toContain("Nantes");
    expect(wrapper.html()).toMatchSnapshot();
  });
  it("emit the 'update' / 'search' event", async () => {
    const wrapper = mount(AddressItem, {
      propsData: {
        address,
      },
    });
    expect(wrapper).toBeDefined();
    const wrapperButtons = wrapper.findAll("button");
    expect(wrapperButtons).toBeDefined();
    wrapperButtons.at(0).trigger("click");
    expect(wrapper.emitted()).toHaveProperty("update");
    expect(wrapper.emitted()).not.toHaveProperty("search");
    expect(wrapper.emitted()["update"]).toHaveLength(1);
    wrapperButtons.at(1).trigger("click");
    expect(wrapper.emitted()).toHaveProperty("search");
    expect(wrapper.emitted()["search"]).toHaveLength(1);
  });
});
