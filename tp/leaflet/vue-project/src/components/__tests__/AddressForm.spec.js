import { mount } from "@vue/test-utils";
import { describe, it, expect } from "vitest";
import AddressForm from "../AddressForm.vue";

const address = {
  label: "My address",
  city: "Nantes",
  zipCode: "44000",
  road: "rue du Marchix",
  state: "France",
};

describe("AddressForm.vue", () => {
  it("renders without crashing", () => {
    const wrapper = mount(AddressForm);
    expect(wrapper).toBeDefined();
    expect(wrapper.html()).toMatchSnapshot();
  });
  it("renders with props", () => {
    const wrapper = mount(AddressForm, {
      propsData: {
        address,
      },
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.html()).toMatchSnapshot();
  });
  it("emit the 'update:addressToEdit' event", async () => {
    const wrapper = mount(AddressForm, {
      propsData: {
        address,
      },
    });
    expect(wrapper).toBeDefined();
    /**
     * find a cell and click on it
     * we add 2 for number of rows / cells
     * because we have a row and a column for Player tips
     * and because we have 0 based arrays
     */
    const wrapperInput = wrapper.find("input");
    expect(wrapperInput).toBeDefined();
    wrapperInput.setValue("pouet");
    await wrapper.vm.$nextTick();
    expect(wrapper.emitted()).toHaveProperty("update:road");
    expect(wrapper.emitted()["update:road"]).toHaveLength(1);
    expect(wrapper.emitted()["update:road"][0][0]).toBe("pouet");
  });
});
