---
title: 7 - Les tests unitaires et d'intégration
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation Makina Sapiens Vue.js

## 7 - Les tests unitaires et d'intégration

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus


---

# Objectifs Les tests unitaires et d'intégration

* Se souvenir d'une histoire classique en test
* Connaître les différents tests
* Découvrir les tests unitaires / composants
savoir manipuler un composant avec jest + test-utils

snapshots, shallow, manipulation d'un composant
* Mettre en place la couverture de code
* Découvrir les tests d'intégrations
* Découvrir les tests de performance

<!--h-->

<!-- .slide: class="title" -->


# Pourquoi tester ?

---

![](http://www.commitstrip.com/shop/45-thickbox_default/affiche-tester-c-est-douter.jpg)

---

# Day one

---

# Month one

---

# Year one

---

![](https://c1.staticflickr.com/3/2203/2245445147_ff54c5997d.jpg)

---

## Les tests servent à ...

* Disposer d'une spécification technique
* Garantir la non régression
* Trouver les erreurs plus rapidement
* Améliorer la qualité du code produit
* Faire évoluer le code plus facilement
* Mieux dormir la nuit !

---

## ... mais ils ne sont pas toujours facile à mettre en oeuvre

* Définir ce qui doit être testé ! (black box !)
* Maintenir la couverture du code définie
* Prendre le temps de tester
* Plus l'application est conséquente, plus les tests sont complexes et indispensables à mettre en place
* Convaincre l'équipe, le client, ...

[Lire l'article Makina par Alex Marandon](https://makina-corpus.com/societe/mythes-et-realites-des-tests-automatises)

<!--h-->

<!-- .slide: class="title" -->


# Définitions

---

Le [Comité français du test logiciel](http://www.cftl.fr/) identifie quatre niveaux de test :

* [Test unitaire](https://fr.wikipedia.org/wiki/Test_unitaire)
* [Test d'intégration](https://fr.wikipedia.org/wiki/Test_d%27int%C3%A9gration)
* [Test système](https://fr.wikipedia.org/wiki/Tests_syst%C3%A8me)
* [Test d'acceptation](https://fr.wikipedia.org/wiki/Test_d%27acceptation)

Source Wikipedia

https://fr.wikipedia.org/wiki/Test_(informatique)

Note:

* [Test unitaire](https://fr.wikipedia.org/wiki/Test_unitaire) (ou test de composants)
* [Test d'intégration](https://fr.wikipedia.org/wiki/Test_d%27int%C3%A9gration)
(anciennement test technique ou test d'intégration technique)
* [Test système](https://fr.wikipedia.org/wiki/Tests_syst%C3%A8me)
(anciennement test fonctionnel ou test d'intégration fonctionnel ou homologation)
* [Test d'acceptation](https://fr.wikipedia.org/wiki/Test_d%27acceptation)
(anciennement test usine ou recette)

---

<!-- .slide: data-background="https://www.commitstrip.com/wp-content/uploads/2014/01/Strips-Le-test-de-ta-mère-650-final.jpg" class="bg-size-contain" -->

---

Autres tests

* test multi-navigateur : [BrowserStack](http://browserstack.com/)
* test fonctionnel : [Cucumber](https://cucumber.io/), [FluentLenium](http://fluentlenium.org/)
* test de sécurité : [ZAP](https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project)
* test de charge : [Gatling](https://gatling.io/), [JMeter](https://jmeter.apache.org/)

<!--h-->

<!-- .slide: class="title" -->

# Tests unitaires

## Test d'une unité, d'un module, d'un composant

---

<!-- .slide: data-background="https://www.commitstrip.com/wp-content/uploads/2017/02/Strip-Ou-sont-les-tests-unitaires-650-final.jpg" class="bg-size-contain" -->

---

# Les méthodologies

## TDD ou ?

Note: Un test doit correspondre aux spécifications de l'application, il faut donc écrire les tests en premier puis les faire passer par la suite plutôt que d'écrire le code avant et de prendre le risque d'être influencé par celui-ci lors de la rédaction des tests 5. Bob Martin6, grand défenseur de la méthode TDD, propose un modèle simple pour l'écriture des tests unitaires :

    Écrire une fonction de test qui doit obtenir un résultat défini dans les spécifications. Ce code appelant un code qui n'existe pas encore, celui-ci doit faillir. Ceci a pour but de définir une fonction qui teste « quelque chose ».
    Écrire le code (le « quelque chose ») pour faire réussir le test.
    Une fois le test en succès, rajouter un autre test pour obtenir un résultat légèrement différent, en faisant varier les entrées par exemple. Ce nouveau test fera faillir le code principal.
    Modifier le code principal pour faire réussir les tests.
    Recommencer, en éliminant et refactorisant les éventuelles redondances dans le code des tests. On refactorise en même temps le code principal que le code des tests.
    Un test unitaire doit tester une caractéristique et une seule. On ne définit pas un « scénario » de test complexe dans un test unitaire.
    Il est déconseillé de tester les fonctions privées d'une classe, on se concentrera à tester les fonctions publiques, c'est-à-dire les interfaces avec lesquelles les acteurs extérieurs interagissent.

(https://fr.wikipedia.org/wiki/Test_unitaire)

Le développement dirigé par les tests est basé sur un cycle à 3 états : Red, Green et Refactor

    Red : écrire un premier test et vérifier qu'il échoue (car le code qu'il teste n'existe pas), afin de vérifier que le test est valide
    Green : écrire juste le code suffisant pour passer le test et vérifier que le test passe
    Refactor : réusiner le code, c'est-à-dire l'améliorer tout en gardant les mêmes fonctionnalités

( https://javacube.fr/2016/03/07/tester-cest-douter/ )

---

<!-- .slide: data-background="https://upload.wikimedia.org/wikipedia/commons/0/0e/Cycle-global-tdd.png" class="bg-size-contain" -->

---

# Fonctionnement

4 phases dans l'exécution d'un TU

setup > test > assert > teardown

* Initialisation / setup
* Exercice / test
* Vérification / assert
* Désactivation / teardown

Note:
* Initialisation (fonction setup) : 
définition d'un environnement de test complètement reproductible (une fixture).
* Exercice : 
le module à tester est exécuté.
* Vérification (utilisation de fonctions assert) : 
comparaison des résultats obtenus avec un vecteur de résultat défini. 
Ces tests définissent le résultat du test : SUCCÈS (SUCCESS) ou ÉCHEC (FAILURE).
On peut également définir d'autres résultats comme EVITE (SKIPPED).
* Désactivation (fonction tearDown) : désinstallation des fixtures pour retrouver l'état initial du système, dans le but de ne pas polluer les tests suivants. Tous les tests doivent être indépendants et reproductibles unitairement (quand exécutés seuls).


---

```js
describe('My Component', () => {

    beforeAll(() => {
        // exécuté une seule fois dans le describe
        // pour l'ensemble des tests
        // au démarrage du describe
    });

    beforeEach(() => {
        // exécuté à chaque tests
        // au démarrage du test
    });

    it('is a Vue instance', () => {
        // le tests à proprement parler

    });

    afterEach(() => {
        // exécuté à chaque tests
        // à la fin du test
    });

    afterAll(() => {
        // exécuté une seule fois dans le describe
        // pour l'ensemble des tests
        // à la fin de tous les tests
    });

});
```

---

# Ce que l'on cherche à tester

* scénario nominal
* edge case
* exceptions

---


# Quelques bibliothèques

* Runners 
[Karma](https://karma-runner.github.io/1.0/index.html)

* Frameworks
[Jasmine](https://jasmine.github.io/),
[MochaJS](https://mochajs.org/),
[JEST (Facebook)](http://facebook.github.io/jest/),
[qUnit (jQuery)](http://qunitjs.com/),
[Enzyme (AirBNB)](http://airbnb.io/enzyme/),
[Chai](http://chaijs.com/),
[Testing Library](https://testing-library.com/),
[Vitest](https://vitest.dev)

* Stubs / mocks / spies 
[SinonJS](http://sinonjs.org/),
[Wiremock](http://wiremock.org/)

---

# Les tests en Vue.js

* Possibilité de faire des tests avec karma / mocha / chai / jasmine en config manuelle
* outil officiel, [vue-test-utils](https://test-utils.vuejs.org/)
* plusieurs moteurs possibles pour jouer les tests, [jest](https://facebook.github.io/jest/), mocha, tape, AVA
* utilise [jsdom](https://github.com/tmpvar/jsdom) dans node
* jest dispose du [snapshot testing](http://facebook.github.io/jest/docs/en/snapshot-testing.html#content) ce qui simplifie en partie l'écriture des tests
* mocking
* code coverage
* isolation et 'sandboxing' des tests

[Tutoriaux](https://alexjoverm.github.io/series/Unit-Testing-Vue-js-Components-with-the-Official-Vue-Testing-Tools-and-Jest/)

---

# Comment tester ?

* **quoi ?**
    * le rendu des composants (snapshot), que l'on peut considérer comme des boîtes noires
    * les services, helpers....
    * les différences de rendu selon le state
* **où ?**
    * fichiers juxtaposés
    * répertoire centralisant les tests
* **quand ?**
    * en permance dans le développement (extension code, console ouverte...)
    * dans les étapes de build
* **comment ?**
    * avec des [mocks](https://facebook.github.io/jest/docs/mock-functions.html#content) : `jest.fn`, ̀`jest.mock`
    * en atteignant une bonne couverture de code (code coverage)
    * en privilégiant du shallowMount (test du composant seulement, pas de ses enfants)


---

# Détails des syntaxes

## jest

Utilisé pour orchestrer les tests

[jest](https://facebook.github.io/jest/)

## vue-test-utils

Utilisé pour analyser l'état des composants

[`@vue/test-utils`](http://vue-test-utils.vuejs.org/) et l'[API](https://vue-test-utils.vuejs.org/api/)

---

<!-- .slide: class="alternate" -->

# [Picross/Leaflet] Étape 1 : Tester `PicrossEditor` / `Address`

**Consignes**

En partant de la base de code sur la slide suivante,

* vérifier que "de base", il se rend bien
* lui donner des paramètres

**Questions**

* quelle fonction de @vue/test-utils utiliser ?
* quelle fonction de jest utiliser ?

**Prérequis**

* connaître les syntaxes [Jest](https://facebook.github.io/jest/) et [`@vue/test-utils` - vue2](https://vue-test-utils.vuejs.org/api/)/[`@vue/test-utils` - vue3](https://next.vue-test-utils.vuejs.org/)
* avoir son projet préconfiguré avec `@vue/test-utils`

---

<!-- .slide: class="alternate" -->

```js
// à adapter selon le composant testé, PicrossEditor
import { config, mount } from "@vue/test-utils";
import { describe, it, expect } from "vitest";

import PicrossEditor from './PicrossEditor.vue'
// ou
import AddressItem from "../AddressItem.vue";

describe('PicrossEditor.vue', () => {
  it('renders without crashing', () => {
    const wrapper = mount(PicrossEditor)
    expect(wrapper).toBeDefined();
  })
  it('renders with props', () => {
    const wrapper = mount(PicrossEditor, {
        propsData: ...
    })
    expect(...)
  })
})
```

---

# Syntaxe pour faire tourner les tests "en permanence"

La CLI de Vue.js wrappe Vitest et @vue/test-utils via la commande

```
npm run test:unit
```

Les tests sont automatiquement surveillés à chaque modification de fichiers.

---

# Les mocks, stubs & co

ou comment mélanger les concepts...

Le principe à mémoriser est que vous souhaitez piloter le retour de certaines fonctions.

Pour plusieurs motifs :
* renvoyer un certain résultat d'une API sans appeler vraiment l'API,
ce qui vous permet de conserver la maîtrise de l'exécution de votre test
* savoir si une fonction a bien été appelée
* et avec quels paramètres
* injecter des "phénomènes", retour d'une API en erreur, ralentissement des trames réseaux, ordre inversé de l'arrivée des requêtes...

---

# Les fonctions asynchrones 

promises / async / await

Toutes les bibliothèques pour faire des tests asynchrones
ne fonctionnent pas de ma même manière.

Pensez y quand vous faites vos choix de librairies !

Les plus récentes savent gérer l'asynchrone avec les mots clés `async` / `await`,
il y en a même qui savent attendre la fin de l'exécution de votre test détecté asynchrone :-) .

---

# Le pilotage d'un composant en Vue.js

Lorsque vous déclenchez un événement sur un composant, 
Vue.js le détecte et va déclencher, si nécessaire, 
toutes les modifications à apporter au modèle, aux templates,
et rendre cela disponible dans le DOM physique du navigateur.

Cette opération de mise à jour EST asynchrone.

Pour obtenir l'effet de cet événement,
nous devons "attendre" la fin de l'exécution de cette mise à jour de l'application.

Il existe en Vue.js une méthode pour cela : [`Vue.nextTick()`](https://vuejs.org/v2/api/#Vue-nextTick)

Cette méthode est asynchrone, et nous permet de garantir qu'après son exécution,
Vue.js aura terminé la mise à jour de l'application.

On l'utilise de cette manière :

```js
await Vue.nextTick()
```

ou dans un composant

```js
await this.$nextTick()
```

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 2 : Ajouter un test d'événement sur le `PicrossEditor`

**Consignes**

* manipuler votre wrapper pour cliquer sur une cellule du tableau
* vérifier qu'il émet un event `update-picross-data`
* avec les bonnes données !

**Questions**

* quelle méthode d'assertion jest devons nous utiliser pour vérifier la "bonne donnée" ?
* quelle fonction de @vue/test-utils utiliser ?

**Prérequis**

* connaître le principe des fonctions [mocks](https://jestjs.io/docs/en/mock-function-api)

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 3 : Tester `PicrossPlayer`

**Consignes**

* vérifier que "de base" il se rend bien
  * le corriger s'il ne se rend pas bien :-)
* lui donner des paramètres d'exécution (voir slide suivante)

**Prérequis**

* connaître les syntaxes [Jest](https://facebook.github.io/jest/) et [`@vue/test-utils`](https://vue-test-utils.vuejs.org/api/)

---

<!-- .slide: class="alternate" -->

```js
const picrossData = {
  title: '4 x 4',
  data: { rows: [[1], [2], [2], [1]], columns: [[1], [2], [2], [1]] },
  size: { rows: 4, columns: 4 }
}
```

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 4 : Ajouter un test d'événement sur le `PicrossPlayer`

**Consignes**

* vérifier que lorsqu'on termine un picross, l'événement victory sort bien

**Prérequis**

* connaître les syntaxes [Jest](https://facebook.github.io/jest/) et [`@vue/test-utils`](https://vue-test-utils.vuejs.org/api/)
* avoir son projet préconfiguré avec `@vue/test-utils`
* connaître le principe des fonctions [mocks](https://jestjs.io/docs/en/mock-function-api)

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 4 : Ajouter un test d'événement sur le `Address` / `AddressForm`

**Consignes**

* vérifier que lorsqu'on clique sur le bouton edit / search, les événements editAddress et searchAddress sont émis
* vérifier que lorsqu'on clique sur le bouton close ou que l'on modifie l'adresse, les événements close et update:addressToEdit sont émis

**Prérequis**

* connaître les syntaxes [Jest](https://facebook.github.io/jest/) et [`@vue/test-utils`](https://vue-test-utils.vuejs.org/api/)
* avoir son projet préconfiguré avec `@vue/test-utils`
* connaître le principe des [événements émis](https://next.vue-test-utils.vuejs.org/api/#emitted)
* 

---

# Spécificités des composants à état

* fastidieux d'analyser le DOM dans les différents états
* facilité de mettre les composants dans ces différents états
* intérêt du snapshot testing

En jest, le snapshot testing s'effectue avec la méthode
[`toMatchSnapshot()`](https://jestjs.io/docs/en/expect#tomatchsnapshotpropertymatchers-hint).

Il y a un guide sur ce sujet : https://jestjs.io/docs/en/snapshot-testing .

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 5 : Ajouter des snapshots à `PicrossEditor` et `PicrossPlayer`

**Consignes**

* ajouter un snapshot sur un éditeur de 10 x 10
* ajouter un snapshot sur un éditeur de 10 x 10 avec deux cellules activées
* ajouter un snapshot sur un player initialisé
* ajouter un snapshot sur un player terminé

**Questions**

* plus facile / moins facile qu'avec storybook ?

**Prérequis**

* connaître la méthode `toMatchSnapshot()`
* connaître les fonctions de `@vue/test-utils` sur le montage de composant

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 5 : Ajouter des snapshots à `Address` et `AddressForm`

**Consignes**

* ajouter un snapshot sur le composant Address
* ajouter un snapshot sur le composant AddressForm

**Questions**

* plus facile / moins facile qu'avec storybook ?

**Prérequis**

* connaître la méthode `toMatchSnapshot()`
* connaître les fonctions de `@vue/test-utils` sur le montage de composant

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 6 : Tester `Game`

**Consignes**

* mocker le résultat `picross.json` pour garantir le bon fonctionnement
* s'assurer qu'on a bien l'affichage des jeux
* provoquer le clic sur un picross
* vérifier qu'il est bien affiché
* vérifier que le timer est bien déclenché
* vérifier l'affichage du composant en cas de victoire

**Prérequis**

* connaître le paquet npm [jest-fetch-mock](https://github.com/jefflau/jest-fetch-mock)
* savoir utiliser un vue-router pendant des tests https://vue-test-utils.vuejs.org/guides/#installing-vue-router-in-tests
* maîtriser l'usage de `$nextTick()`
* savoir piloter le wrapper pour déclencher des événements sur des éléments du DOM
* maîtriser les snapshots Jest

---

# La couverture de code 

ou code coverage

* permet de mesurer les branches, fonctions et lignes
* en définissant des métriques, on peut améliorer la qualité
* en intégrant cette mécanique dans un système d'intégration continue,
on maintient la qualité du projet

Avec jest, on peut ajouter l'argument `--coverage` pour disposer d'un premier rapport
sur les fichiers testés.

La couverture doit être configurée sur l'ensemble des fichiers sources,
sauf exception.

---

<!-- .slide: class="alternate" -->

# [Picross/Leaflet] Étape 7 : Utiliser la couverture de code

**Consignes**

* configurer jest pour intégrer la couverture sur le répertoire src/
* accéder aux rapports

**Prérequis**

* connaître la [configuration de Jest](https://jestjs.io/docs/en/configuration#collectcoverage-boolean) sur la couverture

<!--h-->

<!-- .slide: class="title" -->

# Les tests d'intégration

---

# À quoi ça sert ?

Les tests d'intégration sont plus à destination
de tests "fonctionnels", de scénarios d'usage.

Cela permet de simuler plus facilement le comportement d'un utilisateur,
et de "visualiser" le scénario, quelques outils capturant le flux vidéo du scénario.

Quelques outils :

* [nightwatch](https://nightwatchjs.org/) multi navigateur (Selenium dessous)
* [puppeteer](https://pptr.dev/), Chrome headless seulement
* [cypress](cypress.io/), Chrome, Firefox, et chromium based (Brave, Vivaldi, Edge, Canary...)
* [playwright](https://playwright.dev/) de MicroSoft et compatible Firefox / Chrome / Edge

Un article de blog de comparaison : https://blog.checklyhq.com/puppeteer-vs-selenium-vs-playwright-speed-comparison/

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 8 : Mise en place de cypress

**Consignes**

* scénario 1
  * ouvre la page d'accueil
  * capture une première image et fait un match
* scénario 2
  * va sur la page de l'éditeur
  * génère un nonogramme de 10 x 10
  * vérifie la capture
* scénario 3
  * va sur la page du player
  * choisit le 4 x 4
  * vérifie la capture
* scénario 4
  * résout le nonogramme
  * vérifie la capture

**Prérequis**

* connaître la syntaxe d'usage de Cypress

<!--h-->

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 8 : Mise en place de cypress

**Consignes**

* scénario 1
  * ouvre la page d'accueil
  * vérifie la présence du titre
* scénario 2
  * va sur la page About
  * vérifie la présence du titre
* scénario 3
  * va sur la page de recherche
  * vérifie la présence des deux adresses
  * modifie l'adresse to
  * lance la recherche de l'adresse to
  * vérifie que la popup de la carte a bien la bonne adresse affichée

**Prérequis**

* connaître la syntaxe d'usage de Cypress

<!--h-->

<!-- .slide: class="title" -->

# Autres tests

---

## Tests multi navigateurs

avec BrowserStack par ex.

[Démo](http://browserstack.com)

## Tests de performance

Pour mesurer la performance d'une application,
on va essayer de stresser l'application en créant des utilisateurs fictifs.


avec [Apache jMeter](https://jmeter.apache.org/) par ex.


<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [8 - Intégration continue, déploiement et monitoring](/slides/8_Qualité_CICD.md)
