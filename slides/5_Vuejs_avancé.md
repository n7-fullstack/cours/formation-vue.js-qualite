---
title: 5 - Vue.js avancé
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation Makina Sapiens Vue.js

## 5 - Vue.js avancé

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus


---

# Objectifs Vue.js avancé

* Connaître les différents frameworks graphiques
* Connaître gestion centralisée du state (vuex / redux / pinia)
* Comprendre la composition API
* Connaître génération côté serveur (SSR)
* Comprendre les slots / mixins / extends
* Connaître la gestion de l'internationalisation (i18n)
* Connaître provide / inject, Suspense

<!--h-->

<!-- .slide: class="title" -->

# Les Frameworks Graphiques

---

Les plus connus actuellement

* [Prime Vue](https://primefaces.org/primevue/showcase/#/)
* [Element UI](https://element.eleme.cn/#/fr-FR)
* [Vuetify](https://www.vuetifyjs.com)
* [VueMaterial](http://vuematerial.io)
* [VueBootstrap](https://bootstrap-vue.js.org/)

Votre choix doit se porter sur :
* l'aspect visuel que vous souhaitez amener sur votre application
* l'utilité du framework en rapport à vos besoins identifiés !

Si pas de composant à proprement parler,
vous pouvez vous tourner vers des frameworks CSS "utilitaires",
type [Tailwind CSS](https://tailwindcss.com/)

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 2 : Utilisation d'un framework

**Consignes**

* styler tout ça avec le framework de votre choix !


<!--h-->

<!-- .slide: class="title" -->

# La gestion centralisée des données

---

### Les différentes façons de partager de la donnée

https://medium.com/fullstackio/managing-state-in-vue-js-23a0352b1c87

---

### Flux

* Flux est un pattern, un type d'architecture plus qu'une librairie
* Il existe une [implémentation "officielle"](https://github.com/facebook/flux) créee par Facebook, mais elle est très peu utilisée
* [Flux Cartoon](https://code-cartoons.com/articles/a-cartoon-guide-to-flux/) : un blog illustré pour comprendre Flux

### [VueX](https://vuex.vuejs.org/)

* implémentation dérivée de Flux
* permet de créer un **store qui contient un état** (state)
* réagit à des **actions dispatchées**
* modifie l'état du store grâce à des **mutations**
* extensible via **plugins**

### [Pinia](https://pinia.vuejs.org/)

* évolution de VueX, simplifiant l'API
* compatible **vue-devtools**
* extensible également

---

### Fonctionnement initial

---

![Flow Simple](assets/vuejs-simple-flow.png)

---

### Risques

* **des données doivent transiter** entre plusieurs composants
* **trop de composants enfants imbriqués** doivent accéder et/ou modifier le state global

---

### Fonctionnement avec le store pattern

https://vuejs.org/guide/scaling-up/state-management.html

---

### Fonctionnement avec VueX

---

![Flow Simple](assets/lifecycle-vuex.png)

---

# Structure d'un projet Vue.js avec Pinia

```
|- dist                 Build de l'application
|- node_modules         Modules npm
|- public               Assets, index.html, et autres
|- src                  Code source
   |- assets            Assets à intégrer dans l'application
   |- components        Composants simples + stateful
      |- __tests__      Tests des composants (à juxtaposer là où l'on teste)
   |- [helpers]         Méthodes de gestion de données
   |- [router]          Routes de l'application
   |- [services]        Services s'interfaçant à une API par ex.
   |- store             Stores
      |- domainA        Contient le store pour le domainA
      |- domainB        Contient le store pour le domainB
   |- [styles]          Styles (ou intégré dans le composant)
   |- views             Layouts de l'application / containers
|- vite.config.js        Configuration vite
```

---

# Fonctionnement du store

* **le state du store** stocke les données. Il a un état initial défini et est ensuite altéré par les mutations pour changer son état
* **les components** affichent "bêtement" ce qu'ils reçoivent (dumb components)
* **views** fournissent aux composants :
    * des données (à partir du `state`)
    * des events à **dispatcher aux actions**
* **les actions** sont des mécanismes asynchrones, avec par ex. des appels à une API

---

# Les plugins Pinia

On place en général dans les plugins tout ce qui n'a pas sa place,
ni dans les composants/containers, ni dans les actions.

Un plugin peut par exemple servir à :

* **gérer le localstorage**
* **afficher des logs**

**Le plugin intervient lorsqu'une action est dispatchée**.

Selon le plugin, on peut choisir d'intercepter uniquement certaines actions.

L'action continue ensuite sans se soucier du middleware.

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 2 : Mise en place d'un store

**Consignes**

* Ajouter un **store** au projet
  * qui persiste le temps chronométré du joueur (`time`)
  * qui propose plusieurs **actions** pour mettre à jour le `time`
  * qui propose une **action asynchrone** pour démarrer le timer
  * et une autre pour l'arrêter
  * ajouter un plugin de logger (`vuex/dist/logger`)
  * **dispatcher les 2 actions** dans le store, observer le résultat

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 3 : Câblage du store

**Consignes**

* la connection du store à un **composant** / **container** => smart component
* la mise à jour du state depuis le composant (**mapState, mapDispatch**)
* connecter le timer de la view `Game`

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 2 : Mise en place d'un store

**Consignes**

* Ajouter un **store** au projet
  * qui persiste dans un state les même données que le composant `Search`
  * qui propose une action `fetchCoordinates`
* Le câbler au composant `SearchView.vue`
* L'utiliser dans `App.vue` pour afficher la ville `from` et la ville `to`

**Prérequis**

* comprendre la fonction [reactive](https://v3.vuejs.org/guide/reactivity-fundamentals.html)
* connaître les principes de base de la [gestion centralisation de la donnée](https://v3.vuejs.org/guide/state-management.html)

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 3 : Créer une computed property

**Consignes**

* Rajouter au niveau de notre composant `SearchView.vue` une computedq qui permettra de connaître le nombre de résultats
* Gérer la computed de notre `SearchView.vue` dans le store ?

<!--h-->

<!-- .slide: class="title" -->

# La composition API

---

## Le pourquoi

Avec des applications de plus en plus complexes,
l'expérience des développeurs VueJS a amené une **réflexion**
sur l'amélioration de la maintenabilité du code
dans les différents composants.

L'idée principale, mutualiser et regrouper du code dans une fonction `setup`,
où l'écrivain et le lecteur pourront mieux comprendre la logique applicative, 
au lieu d'éclater la logique du code via l'Option API.

---

## Le comment

La propriété `setup` est une fonction,
qui n'a pas accès à l'instance du composant.

Son exécution est avant l'exécution de la propriété `data`,
mais après récupération des `props`.
Au niveau du cycle de vie, nous sommes entre `beforeCreate` et `created`.

Les `methods`, `computed` ne sont pas encore accessibles non plus.

Et cela pourra être le rôle de cette fonction
que de définir les logiques applicatives (via des données de modèles, des méthodes)
pour les renvoyer en sortie de fonction.


---

Il est également possible d'utiliser une balise `<script setup>`.

```vue
<template>
  {{ state.toto }}
  {{ state.pouet }}
  {{ isTotoTiti }}
  {{ myProp }}
  <button @click="doSomething">Do something !</button>
  <button @click="emit('broadcast')">Broadcast !</button>
</template>

<script setup>
import { reactive, computed, defineProps, defineEmits } from 'vue'

const props = defineProps({
  myProp: String
})

const emit = defineEmits(['broadcast'])

const state = reactive({ toto: 'titi', pouet: 'pouic' })

function doSomething () {
  state.toto = 'tata'
}

const isTotoTiti = computed(() => state.toto === 'titi')
</script>
```

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 4 : Mise en oeuvre de la Composition API

**Consignes**

* dans notre composant `SearchView.vue`
  * réécrire la déclaration du composant en Composition API

**Questions**

* quelle méthodologie employer ?
* quelles fonctions utilitaires de VueJS utiliser ?
* comment reprendre le code du hook `created` ?

---

# Les composables

En Vue 3, la Composition API a également apporté
une nouvelle façon de factoriser son code : les composables.

Il est désormais possible de mutualiser une partie de son code
dans des fichiers "composables", 
qui peuvent être utilisés à divers endroits de notre code.

Cette factorisation a permis également l'émergence
de paquets dédiés à :
* la capture d'événement, souris, clavier, ....
* l'analyse de position GPS du navigateur,
* 

Ces composables ont pour la plupart été intégrés dans
[VueUse](https://vueuse.org/).

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 5 : Extraire du `setup` un composable

**Consignes**

* À partir du store généré précédemment,
* re écrire une fonction `setup` reprenant la logique définie dans le store
* extraire les morceaux de code de la fonction `setup` dans un composable `useAddress`
* utiliser ce même composable `useAddress` dans le composant `App.vue` : que peut on remarquer ?

**Questions**

* y a t'il une différence sur la réactivité entre un store et un composable ?
  
**Prérequis**

* comprendre la [Composition API](https://v3.vuejs.org/guide/composition-api-introduction.html)


<!--h-->

<!-- .slide: class="title" -->

# Le Server Side Rendering

---

### Pourquoi ?

* **le SEO**
* le temps de rendu initial d'une page / webapp

### Quelles solutions ?

* [**nuxt.js**](https://nuxtjs.org/),
bootstraper de projet Vue.js, comme vue-cli
* [**vue-ssr**](https://ssr.vuejs.org/),
brique supplémentaire pour intégrer le SSR
* [**vue-cli-plugin-ssr**](https://vue-cli-plugin-ssr.netlify.com/)
* [**@uvue/ssr**](https://universal-vue.github.io/docs/)
* [**Vapper JS**](https://vapperjs.org/)

---

### Comment ça marche ?

* 1 La page web est demandée au serveur (node)
* 2 la page web est générée par le serveur
* 3 le DOM et la CSS sont livrés par le serveur, la page est rendue côté utilisateur
* 4 le JS arrive ensuite, livré par le serveur
* 5 le JS se câble à l'état de l'application (hydratation)
* 6 la page est interactive, et l'utilisateur peut s'en servir

---

<!-- .slide: class="alternate" -->

# Étape 6 : Ajout du SSR avec vue-ssr

**Consignes**

* en ajoutant les dépendances `vue-server-renderer` et `express`
* rendre la page d'accueil avec un serveur node et `vue-ssr`

* rendre la page `search/` en préchargeant les données nominatim avec l'url `search/:address`

<!--h-->

<!-- .slide: class="title" -->

# slots / extends / provide/inject

### L'évolutivité des composants

---

[Documentation slot](https://v3.vuejs.org/guide/component-slots.html#slots)

* le slot est une partie d'un composant pouvant recevoir des éléments d'un parent : cela s'appelle la **transclusion**
* un composant peut avoir plusieurs slots, qui seront nommés
* chacun de ces slots peut à son tour exposer des données à celui qui instanciera le composant
  * par ex., un composant tableau `table` qui permettrait de définir l'affichage de la ligne `tr`
  * lorsque nous définissons le type de `tr`, nous pourrons avoir accès à la donnée de la ligne courante
  * nous appelons cela un [scoped slot](https://v3.vuejs.org/guide/component-slots.html#scoped-slots)

---


[Documentation extends](https://vuejs.org/api/options-composition.html#extends)

[Suspense](https://vuejs.org/guide/built-ins/suspense.html)

[Teleport](https://vuejs.org/guide/built-ins/teleport.html)

---

[Documentation provide/inject](https://v3.vuejs.org/guide/component-provide-inject.html)

* possibilité d'injecter des `props` à des enfants de composant "en profondeur"
* évite de fournir les props enfant après enfant
* pose des souci sur "d'où vient la donnée" ?

---
<!-- .slide: class="alternate" -->

# [Picross] Étape 5 : Mise en oeuvre des slots / mixins / extends

à réviser avec Vue3 et la Composition API plutôt...

extend déjà vu dans Player <> Game
<!--
* ajouter un slot au composant Map
* l'afficher comme une popup permanente
* rendre dynamique son contenu, par rapport à l'adresse survolée
-->

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 6 : Mise en oeuvre des slots

**Consignes**

* définir un slot `popup` au niveau du composant `Map`
* l'afficher comme une popup permanente
* rendre dynamique son contenu, pour afficher le détail de l'adresse survolée

**Prérequis**

* savoir définir un slot dans un composant
* savoir remplacer la valeur par défaut d'un slot dans le composant parent

<!--h-->

<!-- .slide: class="title" -->

# Chargement asynchrone des composants

lazy / async loading

---

Une SPA peut présenter un nombre importants de composants.

Tous ne sont pas utiles au démarrage de l'application.

Pour optimiser le chargement du JavaScript,
et accélérer le démarrage de l'application,
il est possible de charger à la volée les composants.

Ainsi, seuls ceux qui sont nécessaires sont chargés.

En pratique, il s'agit d'une fonction renvoyant un import de fichier.

```js
new Vue({
  // ...
  components: {
    'my-component': () => import('./my-async-component')
  }
})
```

[Documentation](https://vuejs.org/v2/guide/components-dynamic-async.html#Async-Components)

---

<!-- .slide: class="alternate" -->

# [Picross / Leaflet] Étape 6 : Charger une page en asynchrone

**Consigne**

* rendre la page Game / Search chargée en asynchrone (si pas déjà fait)

<!--h-->

# L'internationalisation

avec [**vue-i18n**](https://vue-i18n.intlify.dev/)

---

<!-- .slide: class="alternate" -->

# [Picross / Leaflet] Étape 7 : Traduire l'application en FR et EN

**Consignes**

* en suivant les consignes pour installer vue-i18n,
* intégrer un fichier de localisation fr_FR et en_EN
* utiliser des répertoires `src/lang/fr_FR` et `src/lang/en_EN` par ex.
* ajouter un bouton permettant de switcher de langue sans rechargement
* traduire les liens de la page, ainsi que quelques mots d'une page en particulier (Search, Game, ...)


<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [6 - Qu'est ce qu'un storybook](/slides/6_Qualité_storybook.md)

