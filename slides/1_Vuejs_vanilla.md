---
title: 1 - Vue.js Vanilla
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation Makina Sapiens Vue.js

## 1 - Vue.js Vanilla

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus


---

# Objectifs Vue.js Vanilla

* Comprendre ce qu'est Vue.js
* Connaître la structure d'une instance Vue.js et son cycle de vie
* Connaître et mettre en pratique quelques directives
  * v-bind
  * v-for
  * v-on
  * v-model
* Savoir utiliser vue-devtools
* Appréhender le système de réactivité Vue.js

<!--h-->

# Qu'est ce que Vue.js ?

[Portail officiel](https://vuejs.org)

**Vue.js est un framework progressif pour créer des Interfaces Utilisateurs (User Interfaces).**

Il **facilite** le travail du développeur
en proposant une **architecture d'application**
ainsi que des **méthodes / utilitaires** rendant moins verbeux
l'écriture des **interactions avec le DOM ou des API**.

---

## **La donnée devient réactive**

Vous la définissez, et si vous la mettez à jour,
tous les éléments (DOM, données, ...) qui sont "reliés" à elle
se mettront automatiquement à jour.

De fait, **la donnée devient la source d'origine**,
et le DOM est généré à partir de la donnée.

---

## **Vous choisissez les briques**

Un routeur ? Il y a `vue-router`.

De la traduction ? Il y a `vue-i18n`.

Une gestion de store centralisée ? Il y a `pinia`.

Tout cela constitue le **framework** Vue.js.

**Progressif**, car vous faites vous même votre recette,
avec des briques "validées" par la communauté.

---

# Un exemple ?

[Introduction à Vue.js, documentation officielle](https://vuejs.org/guide/introduction.html#getting-started)

---

# En résumé

* on crée une application Vue via `[Vue.]createApp` (en Vue 2, c'était `new Vue()`)
* on utilise un "noeud" du DOM pour accueillir notre application via la méthode `mount()`
* la donnée est réactive
  * quand on la définit dans `data()` (il y a également `computed` et `props`)
  * elle devient accessible dans les autres méthodes via `this.maDonnée`
* quelques directives ont été utilisées
  * `v-bind` ou `:` permet d'attacher la donnée à un attribut d'un élément du DOM
  * `v-on` ou `@` permet d'attacher un event listener
  * `v-model` permet une réactivité en double sens (two-way data binding), très utile pour les formulaires
  * `v-if` permet d'afficher conditionnellement une partie du DOM
  * `v-for` permet de faire des boucles pour créer des éléments du DOM
* Vue.js nous permet de définir des composants
  * qui ont des `props` qui correspondent aux propriétés / attributs d'un élément du DOM
  * ce qui doit nous permettre une meilleure réutilisabilité

<!--h-->

<!-- .slide: class="alternate" -->

# [Picross] Étape 1 : Créer une `Table`

**Consignes**

* afficher un tableau de 6 x 6
* ne pas utiliser de `data`

**Questions**

* quels sont les éléments HTML que nous devons utiliser ?
* quelle directive Vue.js pouvons nous utiliser ?
* construire le tableau de 6 x 6 avec les éléments des questions précédentes

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 1 : Créer un "bloc" d'adresse

**Consignes**

* afficher une `div` avec :
  * une route (`road`)
  * un code postal (`zipCode`)
  * et une ville (`city`)
* sans utiliser Vue.js ou définir un modèle de donnée (fonction `data`)

**Questions**

* quels sont les éléments / tags HTML que nous devons utiliser ?

<!--h-->

# Les instances Vue

[Documentation](https://vuejs.org/guide/instance.html)

[Documentation options](https://fr.vuejs.org/v2/api/#Options-Data)

Une Vue, une application Vue ou une instance de Vue à proprement parler,
est un objet intégrant :

* `data`, réactives lorsqu'elles sont fournies à l'instanciation
* `props`, fournies par le parent
* `computed`, évoluent selon leurs dépendances, peuvent être accomodées de *getter*/*setter*
* `methods`, accessibles aussi depuis le template
* `watch`, permet de déclencher des traitements sur l'évolution d'une variable
* de `hooks` au cycle de vie permettant de déclencher des méthodes

---

```js
export default {
  data () {
    return {
      maPropriete: 'ma valeur',
      monChiffre: 123,
      unBooleen: true,
      unObject: {
        sousProp: 'Une sous propriété'
      }
    }
  },
  props: ['propDuParent'],
  computed: {
    propModifiee () {
      return this.propDuParent + ' modifiée !' // si c'est une string...
    }
  },
  methods: {
    maFonction () {
      // ici, nous pouvons faire un traitement de donnée, un appel Ajax...
    }
  },
  watch: {
    /**
     * Cette fonction sera exécutée à chaque fois que maPropriete est modifiée
     */
    maPropriete (newVal, oldVal) {
      // il est possible d'appeler une méthode, ou d'accéder à toute propriété de l'instance
      this.maFonction()
      console.log(this.propModifiee, this.monChiffre)
    }
  }
}
```

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 2 : Définir une variable réactive

**Consignes**

* utiliser désormais deux variables pour définir le nombre de lignes et de colonnes

**Questions**

* où définir ces deux variables ?
* comment les utiliser ?

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 2 : Définir une variable réactive

**Consignes**

* utiliser désormais une variable du modèle
  * qui s'appelera `from` composée de
    * une route (`road`)
    * un code postal (`zipCode`)
    * et une ville (`city`)
* afficher ces données en lieu et place du texte en dur

**Questions**

* afin d'utiliser Vue.js, quelle syntaxe pourrions nous utiliser ?

<!--
* div, br
* interpolation d'un texte https://vuejs.org/v2/guide/syntax.html#Text
-->


<!--h-->

# Le vue-devtools

Il s'agit d'un couteau-suisse du développeur Vue.js.

Développé principalement par @akryum, aka Guillaume Chau.

à installer par ici : https://github.com/vuejs/vue-devtools/

<!--h-->

<!-- .slide: class="title" -->

# La réactivité

---

![](assets/reactivity.png)

---

Les données fournies initialement sont transformées
avec des [`Proxy`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy)
avec des **get** / **set** sur chaque objet, propriété, sous-objet, ...

**Un `Proxy` est un objet qui encapsule un autre objet
et nous permet d'intercepter toute interaction / accès à cet objet.**

(en Vue 2, on utilisait la fonction [`defineProperty`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty))

Les données deviennent observables.

Vue.js peut détecter les changements de ces données,
et met à jour le DOM en cas de changement effectif,
ou appelle tout "effet" nécessaire au changement de ces données.

Seules les données fournies initialement sont concernées.

Il est également possible de créer un objet réactif grâce à la méthode
[`[Vue].reactive`](https://vuejs.org/api/reactivity-core.html#reactive).

Pour les primitives, (string, number, ...) [`[Vue].ref()`](https://vuejs.org/api/reactivity-core.html#ref) sera à privilégier.

Ces deux méthodes seront utilisées dans la **Composition API**.


---

```javascript
// Premier exemple d'application
const app1 = Vue.createApp({
  data () {
    return {
      a: 1
    }
  }
})
const vm1 = app1.mount('#app1')

vm1.a === vm1.$data.a // true
vm1.a = 2
vm1.$data.a // 2

vm1.b = 3
vm1.$data.b // undefined
vm1.$data.b = 4
vm1.b // 4 !
```

---

```js
// Deuxième exemple, en passant par un objet tiers
const data2 = {
  pouet: 'pouic'
}
const dataReactive2 = Vue.reactive(data2)

data2 === dataReactive2 // ?

const app2 = Vue.createApp({
  data () {
    return dataReactive2
  }
})
const vm2 = app2.mount('#app2')

vm2.$data === data2 // false
vm2.$data === dataReactive2 // true

data2.pouet = 'plouf'
vm2.$data.pouet
vm2.pouet
dataReactive2.pouet

dataReactive2.pouet = 'pouic'
// toute les propriétés ont changé

dataReactive2.toto = 'tata'
data2 // contient toto
vm2.toto // vaut tata
vm2.$data.toto // idem
```

---

<!-- .slide: class="alternate" -->

# Essais sur notre code

**Consignes**

* modifier par exemple les variables définissant le nombre de colonnes et de lignes (picross) ou l'adresse (leaflet)
  * via la console
  * via le devtools
  * via une variable `const data` extérieure à l'application Vue.js

---

# à mémoriser

* toute donnée rendue réactive est `Proxy`fiée
* une fois une donnée rendue réactive, VueJS réutilisera le **même** Proxy à chaque appel de `[Vue].reactive`
* chaque modification de donnée réactive est détectée par VueJS qui peut déclencher une mise à jour de toutes les dépendances de la donnée modifiée
  * qu'elle soit visuelle (DOM)
  * ou liée au code (watch, computed, ...)
* ce "state" réactif est accessible dans le template, et dans tout le composant à travers les autres options de l'instance
* lisez https://vuejs.org/guide/extras/reactivity-in-depth.html !


<!--h-->

<!-- .slide: class="title" -->

# Les directives

---

# Les directives

Les directives sont des "attributs" spéciaux des éléments Vue.js.

Ils donnent accès à des fonctionnalités pratiques,
et accélèrent le développement.

[Documentation](https://vuejs.org/v2/api/#Directives)

* [v-bind ](https://vuejs.org/guide/essentials/template-syntax.html)
* [v-show / v-if / v-else / v-else-if](https://vuejs.org/guide/essentials/conditional.html)
* [v-model](https://vuejs.org/v2/guide/forms.html)
* [v-on](https://vuejs.org/v2/guide/events.html#Listening-to-Events)
* [v-for](https://vuejs.org/v2/guide/list.html)
* [v-text / v-html](https://vuejs.org/v2/api/#v-text)
* [v-once](https://vuejs.org/v2/api/#v-once)
* [v-pre](https://vuejs.org/v2/api/#v-pre)
* [v-cloak](https://vuejs.org/v2/api/#v-cloak)

[Exemple d'utilisation de `v-model` dans des formulaires](https://blog.logrocket.com/an-imperative-guide-to-forms-in-vue-js-7536bfa374e0)

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 3 'facile' : Modifier l'aspect d'une cellule

**Consignes**

* activer/désactiver une cellule lorsque le joueur clique dessus (utiliser la classe `.selected` déjà fournie)

**Questions**

* quelle(s) directives utiliser ?
* comment changer "facilement" l'apparence d'une cellule ?
* comment changer l'apparence de la cellule en respectant la philosophie Vue.js ? (la donnée génère le DOM, pas l'inverse)

<!--
* directives : v-on / @ + v-bind / :
* facilement, on mute le DOM
* philosophiquement, on devrait avoir un tableau qui contient la donnée des cellules activées
-->

---


<!-- .slide: class="alternate" -->

# [Picross] Étape 4 Dessiner dynamiquement le tableau du nonogramme

**Consignes**

* ajouter un formulaire de saisie d'un nombre de colonne et de lignes
* générer le tableau à partir des données saisies par le joueur
* faire en sorte qu'il soit mis à jour dynamiquement en cas de modification des données

**Questions**

* quelle(s) directives utiliser ?
* quel(s) élément(s) DOM devons nous utiliser pour disposer d'un champ de saisie ?

**Prérequis**

* key modifier https://vuejs.org/v2/guide/forms.html

<!--
  * v-model
  * input type=text
  * key modifier
-->

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 3 : Affichage conditionnel de données

**Consignes**

* ajouter une deuxième adresse `to`
* ajouter de nouvelles propriétés facultatives `complement`, `country` et `state` aux adresses `from` et `to` en mode "panaché"
* afficher ces nouvelles propriétés si elles sont renseignées


**Questions**

* quelle(s) directives utiliser ?
* quel(s) éléments/tags HTML utiliser ?

<!--
* directives : v-if
* éléments : ul / li ou span
-->

---


<!-- .slide: class="alternate" -->

# [Leaflet] Étape 4 : Créer un formulaire

**Consignes**

* créer un formulaire qui modifie l'ensemble de l'adresse form

**Questions**

* quelle(s) directives utiliser ?
* quel(s) élément(s) DOM devons nous utiliser pour disposer d'un champ de saisie ?

**Prérequis**

* key modifier https://vuejs.org/v2/guide/forms.html

<!--
  * v-model, v-bind
  * input type=text
  * key modifier number
-->

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 4 bonus : Transformer le formulaire statique en généré dynamiquement

**Consignes**

* créer un formulaire "générique" de modification de l'adresse
  * définir l'ensemble des champs d'une adresse
  * générer l'ensemble des `<input>`
  * afficher les champs de l'adresse 
* essayer de `bind`er un `label`, le `type` de l'input

**Questions**

* quelle(s) méthode(s) adopter ?
* quelle(s) directives utiliser ?

<!--
  * v-for, v-bind
  * input type=text
  * key modifier number
-->

<!--h-->

<!-- .slide: class="title" -->

# Le cycle de vie d'une Vue

---

![](assets/lifecycle.png)

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 5 Utiliser le hook created

**Consignes**

* modifier le nombre de colonnes dans le hook `created`

**Questions**

* pourquoi le faire dans le hook `created` ?
* aurions nous pu le faire dans le hook `beforeCreate` ?
* aurions nous pu le faire dans le hook `beforeMount` ?

---

<!-- .slide: class="alternate" -->

# [Picross] Rewind Étape 3 'Vue.js' : Modifier l'aspect d'une cellule, via les données

**Consignes**

* activer/désactiver une cellule lorsque le joueur clique dessus (utiliser la classe `.selected` déjà fournie)
* ajouter un bouton pour que la prise en compte du changement du nombre de lignes / cellules ne soit pas "automatique"

**Questions**

* quelle(s) directives utiliser ?
* comment changer l'apparence de la cellule en respectant la philosophie Vue.js ? (la donnée génère le DOM, pas l'inverse)

**Prérequis**

* this.$set pour modifier des éléments d'un tableau
* savoir écrire une méthode d'une instance Vue
* connaître les hooks d'une instance Vue

<!--
* directives : v-on / @ + v-bind / :
* facilement, on mute le DOM
* philosophiquement, on devrait avoir un tableau qui contient la donnée des cellules activées
-->

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 6 : Générer un nonogramme en JSON

**Consignes**

* ajouter un input permettant la saisie d'un "titre" du nonogramme
* construire un objet JSON qui contiendra
  * le titre du nonogramme
  * les données des cellules (chaque cellule valant, soit `true` soit `false`)
* l'afficher dans la console

**Prérequis**

* savoir écrire une méthode d'une instance Vue

---

<!-- .slide: class="alternate" -->


# [Picross] Étape 7 : construire un objet contenant les instructions d'un nonogramme

Rappeler définition d'un nonogramme

**Consignes**

* construire un objet JavaScript qui contient
  * pour chaque ligne et pour chaque colonne
  * les séquences de chiffres permettant d'arriver au dessin en sortie
  * voir propriétés indiquées page suivante
* afficher cet objet dans un élément `pre`
* enregistrer ce fichier sur son poste en tant que 'picross1.json'

**Prérequis**

* fonction en notation raccourcie, arrow function (facultatif)
* manipulation d'un tableau JavaScript `Array`
* template string (facultatif)
* manipulation du DOM
* algorithmique !
* connaître le principe des `computed` property 
<!-- utiliser `innerHTML`, propriété d'un `Node` DOM -->

<!--
Pièges à éviter

* renseigner la propriété `size` avec les nombres de lignes / colonnes 'corrects', càd issues de l'élément `table` et de ses `tr` / `td` au lieu de se baser sur les champs de saisies qui peuvent changer dans le temps

-->

---

<!-- .slide: class="alternate" -->


```
  {
    "title": "Le titre du nonogramme",
    "data": {
      "rows": [
        [ 1, 2, 3 ], // la première ligne contient 1 puis 2 puis 3 cases "allumées"
        [ 1 ], // la deuxième ligne uniquement une case
        ...
      ],
      "columns": [
        [ 1, 2, 3 ], // la première colonne contient 1 puis 2 puis 3 cases "allumées"
        [ 1 ], // la deuxième colonne uniquement une case
        ...
      ]
    },
    "size": {
      "rows": 10,
      "columns": 10
    }
  }
```

---

<!-- .slide: class="alternate" -->

Détails de l'algorithme (1/2)

**Première étape, recensement de l'état des cellules par ligne et colonnes**
* créer un `Array` pour les `cellsStateByRow`
* créer un `Array` pour les `cellsStateByColumn`
* pour chaque ligne du tableau DOM
  * créer une nouvelle `currentCellsStateRow`
  * pour chaque (`cell`, `cellIndex`) de la ligne courante
    * créer une nouvelle entrée dans `cellsStateByColumn` s'il n'y a pas de colonne correspondante à l'index `cellIndex` de la `cell`
    * ajouter l'état de la `cell` dans la `cellsStateByColumn[cellIndex]`
    * ajouter l'état de la `cell` dans la `currentCellsStateRow`
  * ajouter la `currentCellsStateRow` à `cellsStateByRow`

---

<!-- .slide: class="alternate" -->

Détails de l'algorithme (2/2)

**Deuxième étape, on calcule les enchaînements de cellules activées**
* création d'un tableau pour les `rowsIndications`
* pour chaque `currentCellsStateRow` de `cellsStateByRow`
  * créer une nouvelle `rowIndication`
  * initialisation d'une variable `cellsEnabledNumber` à 0
  * pour chaque valeur de `currentCellsStateRow` (qui correspond à une cellule)
    * si la valeur est "active", on ajoute 1 à `cellsEnabledNumber`
    * sinon
      * si `cellsEnabledNumber` > 0 on l'ajoute à `rowIndication`
      * on remet à 0 `cellsEnabledNumber`
  * si `cellsEnabledNumber` > 0 on l'ajoute à `rowIndication`
  * on ajoute `rowIndication` à `rowsIndications`

Idem pour les colonnes

---

<!-- .slide: class="alternate" -->


# [Leaflet] Étape 5 : ajouter un hook à notre application

**Consignes**

* modifier la ville de l'adresse `from` dans le hook `created`

**Questions**

* pourquoi le faire dans le hook `created` ?
* aurions nous pu le faire dans le hook `beforeCreate` ?
* aurions nous pu le faire dans le hook `beforeMount` ?

---

<!-- .slide: class="alternate" -->


# [Leaflet] Étape 6 : permettre la modification de `from` ou `to`

**Consignes**

* permettre à l'utilisateur de modifier une adresse
* permettre à l'utilisateur de choisir l'adresse à modifier dans le formulaire

**Questions**

* quelle(s) approche(s) pouvons nous choisir ?
* quel élément/tag HTML utiliser ?
* quelle directive Vue.js utiliser ?

<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [2 - Un peu d'histoire](/slides/2_Vuejs_histoire.md)
