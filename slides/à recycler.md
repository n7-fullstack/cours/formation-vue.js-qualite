
<!--h-->

# Utilisation d'une librairie tierce type leaflet

---

<!-- .slide: class="alternate" -->

# Travaux pratiques (step-3)

* intégrer la dépendance leaflet
* ajouter un composant Map en utilisant un fond de carte osm ou jawgs
* découper la page en 1/3 2/3 entre les composants Address / AddressForm et la carte Map

---

<!-- .slide: class="alternate" -->

# Travaux pratiques (step-3.1)

* interroger le service nominatim pour obtenir la correspondance d'une adresse
  * on utilisera la bibliothèque [axios](https://github.com/mzabriskie/axios) qui permet de faire facilement des appels XHR
  * on peut aussi utiliser fetch de la norme avec un polyfill : [https://github.com/github/fetch](https://github.com/github/fetch)
  * à partir de nominatim : [http://nominatim.openstreetmap.org/](http://nominatim.openstreetmap.org/)
  * URL `http://nominatim.openstreetmap.org/search/toulouse?format=json`
* créer un composant stateless `NominatimResult` qui affiche le tableau de résultat nominatim
* au survol d'un des éléments du tableau, ajouter un marqueur à la carte

------
<!-- .slide: class="alternate" -->

# Travaux pratiques (step-3.2)

* interroger le serveur overpass pour récupérer des données des fleuves par ex.
    * on peut aussi chercher à obtenir les parcs pour la zone de Toulouse (step-2.1)
        * à partir d'overpass : [http://wiki.openstreetmap.org/wiki/Overpass_API](http://wiki.openstreetmap.org/wiki/Overpass_API)
        * URL
```
http://overpass-api.de/api/interpreter?data=[out:xml];(way[%22leisure%22=%22park%22](43.54506428956427,1.3108062744140625,43.663525432098275,1.571388244628906););out%20body;%3E;out%20skel%20qt;
```
* afficher dans la console **la liste des éléments** récupérés avec leur coordonnées géographiques
* afficher les résultats de la requête overpass sous forme de geojson
* réagir au déplacement de la bounding box (event zoomend & moveend)
* lors du changement de la bbox, réagir et recharger les données

---


# Tests unitaires


# Mise en pratique

---

[Découverte du projet Pokédex](https://gitlab.enseeiht.fr/mdartigu/tp-testing-pokedex)


---

<!-- .slide: class="alternate" -->

# Travaux pratiques (step-1)

* mise en place de l'environnement de tests
* ajouter la dépendance [jest](https://facebook.github.io/jest/docs/en/getting-started.html)
* ajouter un script npm

---

<!-- .slide: class="alternate" -->

# step-1.1

* écrire un premier test pour la fonction `transformPokemonResponse`
  * ce test vérifie la suppression de certaines propriétés
  * utiliser un test `toBeUndefined`
* corriger les manques
* découvrir le mode watch

------
<!-- .slide: class="alternate" -->

# step-1.2

* ajouter un test qui vérifie l'évolution de `sprites`
* utiliser un test `toBe`
* utiliser un test `not`
* utiliser un test `toContain`

------
<!-- .slide: class="alternate" -->

# step-1.3

* ajouter un test qui vérifie l'évolution de `species`
* utiliser un test `toEqual`

---

## Les fonctions asynchrones 

## promises / async / await

---

### Les mocks, stubs & co

### ou comment mélanger les concepts

---

<!-- .slide: class="alternate" -->

# Travaux pratiques (step-2)

* ajouter un test sur la méthode getPokemon
* utiliser un mock pour axios
* vérifier l'url appelée par la méthode getPokemon
* utiliser un mock pour la méthode de transformation
* tester le fait qu'elle soit bien appelée avec `toHaveBeenCalled`

---

### Spécificités des composants à état

* fastidieux d'analyser le DOM dans les différents états
* facilité de mettre les composants dans ces différents états
* intérêt du snapshot testing

---

<!-- .slide: class="alternate" -->

# Travaux pratiques (step-3)

* ajouter un test sur le composant PokemonDetail
* vérifier l'existence des méthodes `setData`, `emptyDOM`
* créer un premier snapshot vide
* créer un deuxième snapshot avec un pokémon
* créer un troisième snapshot avec deux displays successifs

------
### La couverture de code 

### code coverage

---

* permet de mesurer les branches, fonctions et lignes
* en définissant des métriques, on peut améliorer la qualité
* en intégrant cette mécanique dans un système d'intégration continue,
on maintient la qualité du projet

---

<!-- .slide: class="alternate" -->

# Travaux pratiques (step-4)

* configurer jest pour intégrer la couverture sur le répertoire src/
* accéder aux rapports

---
<!-- .slide: class="title bg-rocks" -->


## Tests d'intégration / fonctionnels

---

<!-- .slide: class="alternate" -->

# Travaux pratiques (step-5)

* mettre en place [nightwatch](http://nightwatchjs.org/gettingstarted#installation) sur le projet
* écrire un scénario qui
  * ouvre la page
  * vérifie la présence de 20 résultats
  * clique sur un bouton
  * vérifie le bon chargement du pokémon

------
<!-- .slide: class="alternate" -->

# step-5.1

Même chose avec cypress.io

---
<!-- .slide: class="title bg-rocks" -->


## Autres tests

---

### Tests multi navigateurs

avec BrowserStack par ex.

[Démo](http://browserstack.com)

------
## Tests de performance

avec Apache jMeter par ex.

<!--h-->

Pour aller plus loin

* https://auth0.com/blog/bootstrapping-a-react-project/
* https://medium.com/@kentaromiura_the_js_guy/jest-for-all-episode-1-vue-js-d616bccbe186#.ir0vdz2mw

<!--h-->

[Retour au sommaire](/index.md#/3/1)





<!--h-->

<!-- .slide: class="title bg-lenin" -->

# Les tests en Vue.js

---

# Les tests en Vue.js

* Possibilité de faire des tests avec karma / mocha / chai / jasmine en config manuelle
* outil officiel, [vue-test-utils](http://vue-test-utils.vuejs.org/)
* plusieurs moteurs possibles pour jouer les tests, [jest](https://facebook.github.io/jest/), mocha, tape, AVA
* jest dispose du snapshot testing, ce qui simplifie en partie l'écriture des tests
* utilise [jsdom](https://github.com/tmpvar/jsdom) dans node
* [snapshot testing](http://facebook.github.io/jest/docs/en/snapshot-testing.html#content)
* tests 'classiques'
* mocking
* code coverage
* isolation et 'sandboxing' des tests

[Tutoriaux](https://alexjoverm.github.io/series/Unit-Testing-Vue-js-Components-with-the-Official-Vue-Testing-Tools-and-Jest/)

---

# Comment tester ?

* **quoi ?**
    * le rendu des composants (snapshot), que l'on peut considérer comme des boîtes noires
    * les services, helpers....
    * les différences de rendu selon le state
* **où ?**
    * fichiers juxtaposés
    * répertoire centralisant les tests
* **quand ?**
    * en permance dans le développement (extension code, console ouverte...)
    * dans les étapes de build
* **comment ?**
    * avec des [mocks](https://facebook.github.io/jest/docs/mock-functions.html#content) : `jest.fn`, ̀`jest.mock`
    * en atteignant une bonne couverture de code (code coverage)
    * en privilégiant du shallowMount (test du composant seulement, pas de ses enfants)

---

# Ecriture d'un test

Setup > Exercise > Verify > Teardown

```javascript
describe('My Component', () => {

    beforeAll(() => {
        // exécuté une seule fois dans le describe
        // pour l'ensemble des tests
        // au démarrage du describe
    });

    beforeEach(() => {
        // exécuté à chaque tests
        // au démarrage du test
    });

    it('is a Vue instance', () => {
        // le tests à proprement parler

    });

    afterEach(() => {
        // exécuté à chaque tests
        // à la fin du test
    });

    afterAll(() => {
        // exécuté une seule fois dans le describe
        // pour l'ensemble des tests
        // à la fin de tous les tests
    });

});
```

---

# Détails des syntaxes

### jest

Utilisé pour orchestrer les tests

[jest](https://facebook.github.io/jest/)

### vue-test-utils

Utilisé pour analyser l'état des composants

[vue-test-utils](http://vue-test-utils.vuejs.org/)

---

<!-- .slide: class="alternate" -->

# Travaux pratiques (step-5)

* écrire un test et vérifier qu'il passe avec `npm run test:unit`
* ajouter un test de rendu pour le composant Home
* ajouter un snapshot pour Home
* ajouter un snapshot pour le composant Nominatim
* ajouter un snapshot pour le composant Map

---

<!-- .slide: class="alternate" -->

# Travaux pratiques (step-5.1)

* mise en place de la couverture de code
* ajouter tests pour nominatim (et overpass)
* améliorer la couverture ?

---

# Solution manuelle

* installer les dépendances

```
npm install -D vue-test-utils jest jest-vue babel-jest jest-serializer-vue
```

* ajouter le fichier `jest.config.js`

```
module.exports = {
  moduleFileExtensions: [
    'js',
    'jsx',
    'json',
    'vue',
  ],
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
    '^.+\\.jsx?$': 'babel-jest',
  },
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  snapshotSerializers: [
    'jest-serializer-vue',
  ],
  testMatch: [
    '<rootDir>/(tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx))',
    '<rootDir>/(tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx))',
  ],
};
```

---

# Solution (suite)

s'assurer de la conf du .babelrc qui précise bien

    !json
    {
        "presets": [
            ["env", { "modules": false }]
        ],
        "env": {
            "test": {
                "presets": [
                    ["env", { "targets": { "node": "current" }}]
                ]
            }
        }
    }
