---
title: 6 - Qu'est ce qu'un storybook
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation Makina Sapiens Vue.js

## 6 - Qu'est ce qu'un storybook

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus


---

# Objectifs Qu'est ce qu'un storybook

* Connaître le principe du storybook
* Savoir écrire des stories
* Connaître les principaux addons

<!--h-->


# Qu'est ce qu'un storybook ?

https://storybook.js.org/

https://histoire.dev

https://www.learnstorybook.com

C'est un outil permettant aux équipes
de développer des composants de manière isolée de l'application principale.

Un storybook devient le "catalogue" des composants d'une application,
et permet de découvrir le potentiel des briques de l'application.

Grâce à des addons, le storybook peut devenir un outil très avancé pour :
* tester des composants (intégration continue, régression visuelle)
* définir des affichages en responsive
* documenter votre projet
* améliorer l'accessibilité de vos composants...

---

# Démo

https://vue3.examples.histoire.dev/

---

# Comment on écrit une 'story' ?

## Storybook

* notion de [Component Story Format](https://storybook.js.org/docs/vue/api/csf)
  * un `export default` qui définit les metadata de vos stories du composant, le composant, des décorateurs, des paramètres...
  * un `export` nommé pour chacune de vos story

## Histoire

https://histoire.dev/guide/vue3/stories.html

---

```js
// MyComponent.stories.js

import MyComponent from './MyComponent.vue';

export default {
  /* 👇 The title prop is optional.
  * See https://storybook.js.org/docs/vue/configure/overview#configure-story-loading
  * to learn how to generate automatic titles
  */
  title: 'Path/To/MyComponent',
  component: MyComponent,
};

export const Basic = () => ({
  components: { MyComponent },
  template: '<MyComponent />',
});

export const WithProp = () => ({
  components: { MyComponent },
    template: '<MyComponent prop="value"/>',
});

```

---

# Histoire

```vue
<!-- Meow.story.vue -->
<script setup>
import Meow from './Meow.vue'
</script>

<template>
  <Story title="My story title">
    <Meow/>
  </Story>
</template>
```

---

# Quelques addons

**Essentials** https://storybook.js.org/addons/tag/essentials/

* Actions : log des interactions utilisateurs
* Viewport : pour développer vos composants en RWD
* Docs : permet de documenter l'usage de vos composants
* Controls : permet de définir des paramètres modifiables pour vos composants

**Autres**

* [Links](https://storybook.js.org/addons/@storybook/addon-links/): permet de relier les stories entre elles
* [a11y](https://storybook.js.org/addons/@storybook/addon-a11y/): permet d'avoir des conseils sur l'accessibilité de vos composants
* [storyshots](https://storybook.js.org/addons/@storybook/addon-storyshots/): permet de déclencher la capture DOM et visuelle de vos composants, utile pour l'intégration continue

---

Quelques storybook en cours de développement :

* [MFS](http://mfs-storybook-master.surge.sh)
* [LocoKit](http://lck-storybook.surge.sh/)

---

<!-- .slide: class="alternate" -->

# [Picross/Leaflet] Étape 1 : mettre en place un storybook

**Consignes**

* configurer le projet pour qu'il accueille un storybook

```
npx sb init
```

* démarrer avec `npm run storybook`

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 2 : écrire des stories pour l'Editor

**Consignes**

* écrire une story pour le composant `PicrossEditor`
* fixer les propriétés colonnes / lignes (10x10)

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 3 : écrire des stories pour le Player

**Consignes**

* écrire une story pour le composant `PicrossPlayer`
* fournir un JSON de picross au composant pour son affichage
* deuxième story avec un 2ème JSON ?
* afficher les actions effectuées par l'utilisateur dans le panel "actions"

**Prérequis**

* connaître l'addon actions

---
<!-- .slide: class="alternate" -->

# [Leaflet] Étape 2 : écrire des stories pour Address

**Consignes**

* écrire une story pour le composant `Address` par défaut
* écrire une story pour le composant `Address` avec une adresse
* afficher les actions effectuées (edit / search) par l'utilisateur dans le panel "actions"
* permettre la modification des données utilisées pour l'affichage
* faire la même chose pour le composant `AddressForm` (attention à l'événement `update:editToAddress`)

**Prérequis**

* connaître l'[addon actions](https://storybook.js.org/docs/vue/essentials/actions)
* connaître l'[addon controls](https://storybook.js.org/docs/vue/essentials/controls)

---
<!-- .slide: class="alternate" -->

# [Leaflet] Étape 3 : écrire des stories pour Map, About, Home

**Consignes**

* écrire une story pour le composant `Map` par défaut
* écrire une story pour le composant `Map` avec des données `nominatimResult`
* afficher les actions effectuées (bbox / ?) par l'utilisateur dans le panel "actions"
* écrire une story pour le composant `About`
* écrire une story pour le composant `Home`, en fr et en

**Prérequis**

* connaître l'[addon actions](https://storybook.js.org/docs/vue/essentials/actions)
* connaître l'[addon controls](https://storybook.js.org/docs/vue/essentials/controls)

---

<!-- .slide: class="alternate" -->

# [Picross/Leaflet] Étape 4 : configurer l'addon storyshots et lancer les tests

**Consignes**

* mettre en place l'addon [storyshot core](https://github.com/storybookjs/storybook/tree/next/addons/storyshots/storyshots-core)
* ajouter également les dépendances [jest pour vue3](https://github.com/storybookjs/storybook/tree/next/addons/storyshots/storyshots-core#configure-jest-for-vue-3) ou [jest pour vue](https://github.com/storybookjs/storybook/tree/next/addons/storyshots/storyshots-core#configure-jest-for-vue)
* ajouter un fichier tests/unit/storyshots.spec.js contenant les instructions suivantes

```js
import initStoryshots, { multiSnapshotWithOptions } from '@storybook/addon-storyshots'

initStoryshots({
  framework: 'vue3', // 'vue3' pour du Vue 3, 'vue' pour du Vue 2
  suite: 'storyshots',
  test: multiSnapshotWithOptions()
})
```

* prévoir l'anomalie pour le composant `Map` lié à [l'absence de DOM](https://github.com/storybookjs/storybook/tree/next/addons/storyshots/storyshots-core#disable)
* lancer `npm run test:unit` et observer le résultat

---

<!-- .slide: class="alternate" -->

# [Picross/Leaflet] Étape 5 : configurer l'addon storyshot puppeteer et lancer les tests

**Consignes**

* poursuivre la configuration avec l'addon [storyshot puppeteer](https://github.com/storybookjs/storybook/tree/next/addons/storyshots/storyshots-puppeteer)
* ajouter un fichier tests/unit/imageshots.spec.js contenant les instructions suivantes
* lancer `npm run test:unit` et observer le résultat
* changer une classe CSS dans le fichier `PicrossEditor.vue` 
  * ajouter un `background-color: red` à la règle sur `table`
  * relancer `npm run test:unit` et observer le résultat dans le répertoire `tests/unit/__image_snapshots__/__diff_output__`

---

<!-- .slide: class="alternate" -->

```js
import path from 'path'
import initStoryshots from '@storybook/addon-storyshots'
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer'
import expect from 'expect'
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { configureToMatchImageSnapshot } = require('jest-image-snapshot')

const getMatchOptions = () => {
  return {
    failureThreshold: 0.2,
    failureThresholdType: 'percent'
  }
}

initStoryshots({
  framework: 'vue',
  suite: 'storyshots-puppeteer',
  test: imageSnapshot({
    getMatchOptions,
    storybookUrl: process.env.CI ? `file:///${path.resolve(__dirname, '../../storybook-static')}` : 'http://localhost:6006'
  })
})

// https://github.com/americanexpress/jest-image-snapshot#optional-configuration
const toMatchImageSnapshot = configureToMatchImageSnapshot({
  customDiffConfig: { threshold: 0 }
})

expect.extend({ toMatchImageSnapshot })
```


<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [7 - Écrire des tests](/slides/7_Qualité_TU.md)
