---
title: 2 - Un peu d'histoire
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---


<!-- .slide: class="title logo" -->

# Formation Vue.js

## 2 - Un peu d'histoire

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus

---

# Objectifs Un peu d'histoire

* Connaître un petit historique de Vue.js et des frameworks front
* Connaître les principes fondamentaux de Vue.js
* Connaître les ressemblances avec Angular / React

<!--h-->

<!-- .slide: class="title" -->

# Historique

---

## Dates clés (1)

* *1995* : naissance de **Javascript**
* *2009* : Sortie de **l'iPhone 3GS**, **AngularJS**
* *2013* : Premiers essais de refactorisation d'AngularJS par Evan You
* *2013* : Sortie de la 0.6, première release de Vue.js
* *2014* : Sortie de la 0.11
* *2015* : Sortie de la 1.0 Evangelion
* *2015* : Adoption par la communauté Laravel comme brique front de base

---

## Dates clés (2)

* *2016* : [Campagne Patreon](https://www.patreon.com/evanyou), Evan You full time
* *2016* : Refonte complète avec la 2.0 Ghost In The Shell (Virtual DOM)
* *2017* : 2.5 Level E avec prise en compte des PWA, SSR, async components...
* *2017* : VueConf en Pologne
* *2018* : VueConf qui se multiplient : Amsterdam, US, Barcelone...
* *2020* : Sortie de la v3.0 One Piece, réécrit en TypeScript, usage de Proxy, ~10Kb, 2x plus rapide...
* *2021* : Écosystème du framework compatible avec Vue 3.0

Note:
2016 marque le début de l'envolée de la popularité de vue

Beaucoup d'acteurs gravitent désormais autour de ce framework

https://bestofjs.org/projects?tags=framework

---

## Une popularité GitHub incontestable

* [Bestof JS](https://bestofjs.org/tags/framework/)
* Projet GitHub le plus starré en 2016 / 2017 / 2018 / 2019
* à ce jour, +200k stars, soit plus que React ! :-)
* attention, avec Vue 3 qui est dans un autre repo, les stars ont tendance à se mélanger entre les deux repos...

---

## Des entreprises 'reconnues' qui l'utilisent


|||
|---|---|
| Nintendo | [GitLab](https://about.gitlab.com/2016/10/20/why-we-chose-vue/) |
| Alibaba  | [Facebook](https://newsfeed.fb.com) |
| Adobe  | [Codeship](https://blog.codeship.com/consider-vuejs-next-web-project/) |
| IBM  | Baidu |
| ...  | ... |

[cf listing](https://github.com/vuejs/awesome-vue#enterprise-usage)

Note:
Bref.

Quelques indicateurs qui démontrent
un intérêt prononcé de la communauté pour cette librairie.



<!--h-->

<!-- .slide: class="title" -->

# Les principes clés

---

## Utilisable sans tooling, directement dans le navigateur

---

## Approche composant

---

![first](assets/think-component-1.png)

---

![second](assets/think-component-2.png)

---

## Single File Component

un template + un script + un style

= un composant

---

![first](assets/vue-component.png)

Note:

Possibilité d'utiliser du TypeScript en lieu et place du JS
du SCSS / PostCSS / Less dans la partie style, de les scoper

---

## Virtual DOM

Le Virtual DOM est une représentation du DOM Physique à un instant T.

A chaque modification d'une donnée du modèle,
Le DOM Virtuel permet de connaître les différences qui doivent
être apportées au DOM Physique pour qu'il corresponde
au DOM Virtuel.

---

## Rendu déclaratif
## Système de réactivité

---

![first](assets/reactivity.png)

---

## Two Way Data Binding

Comme AngularJS, il est possible de définir un modèle
et des éléments du template qui sont synchronisés (formulaires).

* le modèle évolue, et le template reflète immédiatement les modifications
* le template évolue, et le modèle reflète immédiatement les modifications


---

## One Way Data Flow

Comme React, le flux de données peut aussi être
strictement descendant.

Les composants obéissent aux données qu'ils reçoivent,
et remontent les évolutions via des méthodes fournies
par les parents ou des events.

---

![one way data flow](assets/vuejs-simple-flow.png)

---

## Expérience développeur

* documentation claire, maintenue au fil des évolutions, traduite dans plusieurs langues
* comparatif entre frameworks sur la doc°
* breaking change peu nombreux, assistés en cas de migration
* courbe d'apprentissage très rapide

Note:
Vue.js est une bonne entrée en matière pour découvrir les frameworks JS

---

## Framework progressif ?

---

![one way data flow](assets/progressiveframework.png)

---

## Projet communautaire

* initié en projet solo par Evan You
* financé par [patreon](https://www.patreon.com/evanyou) et [open collective](https://opencollective.com/vuejs/)
* motivation de la communauté pour créer des **VueConf**
* évangélistes de 'tout bord', MicroSoft, GitLab,...
* ajout compatibilité NativeScript
* de plus en plus de personnes financées par les campagnes Patreon, Open Collective, GitHub sponsors...
* ...


<!--h-->

<!-- .slide: class="title" -->

# Ecosystème Vue.js

---

# Outillage

* *design system*
  * [storybook for vue](https://storybook.js.org/basics/guide-vue/)
  * [**Histoire**](histoire.dev/)
* *routing* : [**vue-router**](https://router.vuejs.org)
* *store* :
  * [vuex](https://vuex.vuejs.org/), basé sur Flux
  * [**pinia**](https://github.com/posva/pinia/)
* *traduction* : [vue-i18n](http://kazupon.github.io/vue-i18n/)
* *tests* :
  * [vue-test-utils](https://vue-test-utils.vuejs.org/) avec jest
  * [**vitest**](vitest.dev/)
* *debug* : [**vue-devtools**](https://github.com/vuejs/vue-devtools/)
* *extensions IDE*
  * *vetur* : extension dispo sur [VS Code](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
  * ***volar*** : extension dispo sur [VS Code](https://marketplace.visualstudio.com/items?itemName=Vue.volar) et [ST3](https://packagecontrol.io/packages/LSP-volar)

---

# [CLI](https://cli.vuejs.org/)

## En mode maintenance

```sh
npm install -g @vue/cli

vue create my-project
```

* basé sur webpack
* plusieurs choix / réutilisation d'un preset
  * TypeScript / Transpiler ES6/7/x / PWA
  * linters (StandardJS, ESLint)
  * preprocessors pour le style (sass / postcss)
  * test unitaires / d'intégrations
  * accepte le JSX
* fonctionne avec des plugins
* permet de surcharger la configuration webpack / ...
* dispose d'une UI basée sur plugins pour piloter plus facilement le projet !

---

# [create-vue](https://github.com/vuejs/create-vue)

```sh
npm create vue@3
```

* basé sur vite
* propose une expérience similaire à la CLI
* plus rapide ! et plus évolutive
* à utiliser désormais

---

# [Nuxt.js](http://nuxtjs.org/)

* Équivalent de next pour React
* facilite la construction de projet Vue.js
* SSR, SPA, sites statiques, ...
* Split automatique du code selon les routes
* Transpilation ES6/ES7
* module [content](https://content.nuxtjs.org/) pour une approche CMS
* version 3 prévue pour fin Octobre 2022

---

# Générateur de site statique

* [Gridsome](https://gridsome.org/) alternative à GatsbyJS
* [Vuepress](http://vuepress.vuejs.org/)
* [Vitepress](http://vitepress.vuejs.org/)

---

# Mobile

* [NativeScript-Vue](https://nativescript-vue.org/)
* [Ionic](https://ionicframework.com/)

---

# Desktop

* [Tauri](https://tauri.studio)
  * [compatible vite](https://tauri.app/v1/guides/getting-started/setup/vite)
* [Electron](https://www.electronjs.org/)
---

# Librairies graphiques

* [Prime Vue](https://primefaces.org/primevue/showcase/#/), compatible Vue3 et sponsor Vue.js
* [vuetify](https://vuetifyjs.com/) basé sur Material
* [Element](http://element.eleme.io/)
* [Bootstrap](https://bootstrap-vue.js.org/)
* [Quasar](http://quasar-framework.org/)
* ...

---

L'écosystème de Vue.js est en constante évolution,
mais il y a un socle qui définit le framework Vue.js
par l'adoption et la maintenance de briques essentielles
au delà de la simple librairie de composant qu'est Vue.js.


<!--h-->

<!-- .slide: class="title" -->

# État du projet

---

## En 2018

* Nuxt.js / NativeScript-Vue sont passés par la version 1.0, et maintenant 2.0
* Vuetify stabilisé en v1
* vue-cli 3.0 avec webpack 4 et vue-loader 15
* vuepress en stabilisation
* évangélisation de Vue.js à travers plusieurs conférences pour aller au contact des dévs
* mise en place de la 2.6-next avec abandon support IE, utilisation des Proxy => plus de caveat pour le système de réactivité

Note:
Projet communautaire, et financé par des campagnes Patreon
Core team réduite, contribution Evan You essentielle, mais stratégie de délégation de plus en plus présente
Communauté indispensable au développement des briques 'tierces', type nuxt, vue-test-utils, vue-devtools, vue-apollo, vue-i18n... traduction des documentation pour plus d'accessibilité (force du côté asiat, mais aussi france etc.)
Communauté FR présente : frères Chopin = Nuxt, Eduardo @posva (espagnol parisien), @akryum

Même un projet qui fait marcher du code React dans Vue & inversement (https://github.com/akxcv/vuera) !

Onboarding facile

---

## 2019

* préparation de la v3
  * avec la [Composition API](https://vue-composition-api-rfc.netlify.com/) (penser mixins réellement réutilisable) [déjà dispo pour la v2](https://github.com/vuejs/composition-api)
  * avec pas (ou peu) de breaking change
  * une reécriture en full TypeScript
  * un découpage modulaire pour faciliter l'abordage des contributeurs
  * 50% plus léger, 100% plus rapide
  * usage de [Proxy](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy) en lieu et place des `defineProperty`
  * build 'spécifique' IE11, mais pas 100% compatible (volonté de cibler les navigateurs 'evergreen')
* vue-cli v4
* vue-devtools v5

---

## 2020

* release de la v3 One Piece
* montée progressive de l'éco système
  * CLI en v5 avec Webpack 5
  * Devtools en v6, encore instable
  * Router en révision pour utiliser la Composition API
  * réflexions en cours sur VueX...
* Ionic Vue officiellement supporté
* Nuxt financé à 1M€ 

---

## 2021

* stabilisation du framework pour usage "en production"
* sortie du bundler [Vite](https://vitejs.dev/)
* montée en puissance de l'écosystème Vue pour compat v3
  * vue-router
  * vuex / pinia
  * i18n
  * frameworks graphiques : PrimeVue, Material UI, Element, Vuetify
* itération sur la mise à jour de VueX, version 5, et pinia

---

## 2022

* Vue 3 version "main"
* Viteconf
* create-vue
* vitest
* to be continued...


<!--h-->

# Les ressemblances avec AngularJS / React

### AngularJS

* directives (ng-repeat, ng-if, ... => v-for, v-if, ...)
* services / helpers
* two way data binding (ng-model => v-model)
* approche par template

### React

* JSX
* Virtual DOM
* approche composant
* one way data flow (props)


<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [3 - La CLI de Vue.js](/slides/3_Vuejs_cli.md)
