---
title: 4 - Le routage front
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---


<!-- .slide: class="title logo" -->

# Formation Makina Sapiens Vue.js

## 4 - Le routage front (vue-router)

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus


---

# Objectifs Le routage front (vue-router)

* Comprendre ce qu'est le routage front
* Savoir créer des routes au sein d'une application
* Connaître la résolution d'une route
* Savoir passer des paramètres à une page
* Connaître les différents modes de navigation (hash vs history)
* Savoir charger dynamiquement des composants
* Connaître la mécanique des transitions CSS dans Vue.js

<!--h-->

# Le routing

Le routing front permet à une application web
de se comporter comme une application "multi-page",
sans rechargement de la page côté serveur.

L'application web peut ainsi disposer de plusieurs sous-parties,
comme un site web, une application à plusieurs fonctionnalités, etc.

Le routeur `Vue Router` est le routeur officiel de l'éco-système Vue.js,
et permet ainsi de développer des **S**ingle **P**age **A**pplication.

[Documentation](https://router.vuejs.org/)

---

Création d'un routeur :

```js
import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
  ],
});

export default router;
```

---

Puis explicitation à Vue de l'usage de ce routeur,

```js
import router from "./router";

...

app.use(router);

```

Au niveau du template, mise à disposition de deux balises :
* `<router-link>` ou `<RouterLink>` : similaire à l'ancre `<a>`
* `<router-view>` ou `<RouterView>` : instancie le composant qui correspond à la résolution de routes

Au niveau du code, mise à disposition de deux variables :
* `$route` : correspond à la route résolue
* `$router`: correspond au routeur Vue.js, il est manipulable programmatiquement

---

Déclaration classique d'une route

```js
const routes = [
  {
    path: '/foo',
    component: Foo
  },
  {
    path: '/bar',
    component: Bar
  }
]
```

Le composant correspondant à la route sera injecté
dans un composant `<router-view />`.

---

Prise en compte des paramètres dans l'URL

```js
const routes = [
  // Les segments dynamiques commencent avec la ponctuation deux-points
  {
    path: '/utilisateur/:id',
    component: User
  } // $route.params.id sera renseigné
]
```

Possibilité de les injecter en props directement

```js
const routes = [
  {
    path: '/utilisateur/:id',
    component: User,
    props: true
  } // id sera renseigné directement au niveau du vm
]
```

---

Concernant les query params (ex: http://xxx/utilisateur **?id=yyy**),
vous n'avez pas à les déclarer dans les routes.

Vous pouvez y accéder via `this.$route.query.id` dans notre exemple.

---

## History mode

Dans une SPA, 
les routes peuvent s'afficher de différentes manières dans la barre d'URL
du navigateur.

Classiquement, on va trouver des URL en **hash mode** :

http://xxx/#/maroute

Autre possibilité, le **history mode** qui permet d'avoir des "jolies" URLs :

http://xxx/maroute

Il y aura un inconvénient, si l'utilisateur fait un "F5" de votre retour,
il aura une belle **404**.

Il y a une configuration à effectuer côté serveur web,
pour renvoyer systématiquement votre fichier `index.html`
et ainsi le laisse gérer l'affichage de la route `/maroute`.

Ne pas oublier de "fabriquer" une fausse 404 dans votre SPA...

https://router.vuejs.org/guide/essentials/history-mode.html

---

Autres possibilités

[Routes imbriquées](https://router.vuejs.org/guide/essentials/nested-routes.html)

[Tutorial](https://medium.com/frontend-fun/vuejs-routing-with-vue-router-1548f94c0575)

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 7 : Découpage de notre application en 3 pages

**Consignes**

* créer une page éditeur
* créer une page jeu (chargée dynamiquement)
* créer une page à propos

**Prérequis**

* savoir créer des composants
* savoir créer des routes

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 7 : Découpage de notre application en 3 pages

**Consignes**

* créer une page d'accueil
* créer une page de recherche (chargée dynamiquement)
* créer une page à propos

**Prérequis**

* savoir créer des composants
* savoir créer des routes

---

Navigation guard

https://router.vuejs.org/guide/advanced/navigation-guards.html#global-before-guards


---

<!-- .slide: class="alternate" -->

# [Picross] Étape 8 : Chronométrer le joueur

**Consignes**

* ajouter un chronomètre et le déclencher au démarrage du choix d'un picross, dans la page jeu
* afficher l'évolution du chronomètre
* arrêter le chronomètre lorsque le joueur a gagné

**Questions**

* comment organiser notre code ?
* que se passe t'il si nous changeons de page au cours du jeu ?

**Prérequis**

* utiliser l'asynchrone avec un `setInterval` et `clearInterval`
* savoir utiliser les hooks du cycle de vie d'une instance Vue

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 9 : Paramétrer la route du Player

**Consignes**

* lorsque le joueur clique sur un titre de picross, le persister dans la route via un query param `picross`
* prendre en considération ce paramètre lors du chargement des picross et déclencher automatiquement la sélection de ce picross

**Questions**

* que se passe t'il si vous cliquez plusieurs fois sur le même titre de picross ?

**Prérequis**

* savoir déclarer et utiliser un paramètre de route dans un composant Vue.js
* savoir piloter le routeur Vue.js programmatiquement
* savoir déclencher un traitement à partir de l'évolution d'une donnée du modèle d'un composant
---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 8 : Paramétrer la route Search

**Consignes**

* rajouter un paramètre de route `address` dans la route `Search`
* prendre en considération ce paramètre lors du chargement de la page et déclencher automatiquement la recherche de l'adresse

**Prérequis**

* savoir déclarer et utiliser un paramètre de route dans un composant Vue.js
* savoir piloter le routeur Vue.js programmatiquement
* savoir déclencher un traitement à partir de l'évolution d'une donnée du modèle d'un composant

**Conseils**

* pour simplifier, on partira du principe que le paramètre dans l'URL sera un objet "conforme" à notre signature : `{ "city": "Nantes", "zipCode": "44000", "road": "11 rue du Marchix", ...}`

<!--h-->

# Les transitions

Vue.js intègre nativement un mécanisme 
qui permet de définir des transitions CSS au niveau de composants.

La [transition CSS](https://developer.mozilla.org/en-US/docs/Web/CSS/transition)
permet de transformer un élément du DOM
en lui appliquant plusieurs règles CSS, qui vont altérer son état.

---

Vue.js "câble" ces classes de transition CSS aux "hooks" du cycle de vie d'une instance Vue.js .

Lorsque l'élément va être injecté dans le DOM (`mounted`),
il va lui appliquer une classe CSS (`xxx-enter`, `xxx-enter-to`, `xxx-enter-active`).

Lorsque l'élément sera démonté, ce sera le même principe, 
avec les classes (`xxx-leave`, `xxx-leave-to`, `xxx-leave-active`).

Cette `transition` pourra être appliquée à un élément,
ou à plusieurs via `transition-group` (cas des `v-for`),
et sera "mise en place" par Vue.js lors de l'apparition / disparition des éléments,
par exemple avec les directives `v-if` / `v-show`.

---

[Documentation](https://vuejs.org/guide/built-ins/transition.html)

[Animation de transition de page](https://codesource.io/animating-between-routes-with-route-transitions-in-vue/)

https://router.vuejs.org/guide/advanced/transitions.html

https://www.smashingmagazine.com/2020/07/css-transitions-vuejs-nuxtjs/

---

![](assets/transition.png)

---

<!-- .slide: class="alternate" -->

# [Picross/Leaflet] Étape 10 : Ajouter une transition de page

**Consignes**

* lorsque l'utilisateur change de page, ajouter une transition CSS avec les règles ci-dessous
* configurer la transition pour favoriser une transition fluide des deux composants

```css
.fade-x-enter-from,
.fade-x-leave-to {
  opacity: 0;
  transform: translateX(20px);
}

.fade-x-enter-active,
.fade-x-leave-active {
  transition: opacity 0.3s, transform 0.3s;
}
```

**Prérequis**

* comprendre le mécanisme des transitions CSS appliqué à Vue.js

<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [5 - Vue.js avancé](/slides/5_Vuejs_avancé.md)
