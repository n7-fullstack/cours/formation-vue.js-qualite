---
title: 8 - Intégration continue, déploiement et monitoring
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation Makina Sapiens Vue.js

## 8 - Intégration continue, déploiement et monitoring

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus


---

# Objectifs Intégration continue, déploiement et monitoring

* Connaître des outils de CI
* Mettre en place une CI
* Connaître des outils de CD
* Mettre en place un CD
* Connaître des outils de monitoring applicatif
* Mettre en place du monitoring applicatif

<!--h-->

<!-- .slide: class="title" -->

# Intégration / Déploiement continu

---

# À quoi ça sert ?

* les projets sont devenus complexes
* la maintenabilité d'un projet dépend de sa qualité...
* automatiser les tâches de lint, test, build, déploiement ... 
* garantit la reproductibilité...
* la pérennité...
* procure un gain de temps !
* et fournit du service à l'équipe dév et client !

---

# Intégration continue

> L'intégration continue est un ensemble de pratiques utilisées en génie logiciel
consistant à vérifier à chaque modification de code source 
que le résultat des modifications ne produit pas de régression 
dans l'application développée.

source: wikipedia fr

---

# Livraison continue

> Continuous delivery (CD) is a software engineering approach 
in which teams produce software in short cycles, 
ensuring that the software can be reliably released at any time.

source: wikipedia en

---

![](assets/ci-diagram.png)

---


# Déploiement continu

Le déploiement continu est la livraison continue avec déploiement automatique 
sur des environnements au delà du développement.

Quelques services d'hébergement accessible gratuitement :

* surge
* netlify
* github pages
* gitlab pages

---

# Liens de documentation

* http://blog.octo.com/continuous-deployment/
* http://blog.octo.com/devops-de-lintegration-continue-au-deploiement-continu/
* https://fr.wikipedia.org/wiki/Int%C3%A9gration_continue
* https://en.wikipedia.org/wiki/Continuous_delivery


---

# Les outils existants

* [Travis CI](https://travis-ci.org/)
* [Gitlab CI](https://docs.gitlab.com/ce/ci/)
* [Circle CI](https://circleci.com/)
* [Codeship](https://codeship.com/)
* [Jenkins](https://jenkins.io/)
* [Sonarqube](https://www.sonarqube.org/)
* [Gradle](https://gradle.org/)

---

<!-- .slide: class="title" -->

# Mise en oeuvre CI / CD

---

<!-- .slide: class="alternate" -->

Par quoi on commence ?

---

<!-- .slide: class="alternate" -->

Intégration continue

---

<!-- .slide: class="alternate" -->

Créons d'abord un `.gitlab-ci.yml`

---

<!-- .slide: class="alternate" -->

un job de lint ?

---

<!-- .slide: class="alternate" -->

un de test unitaire ?

---

<!-- .slide: class="alternate" -->

il fait froid... met ta couverture !

Note:
https://about.gitlab.com/2016/11/03/publish-code-coverage-report-with-gitlab-pages/

---

<!-- .slide: class="alternate" -->

et tes badges !

---

<!-- .slide: class="alternate" -->

on joue avec cypress ?

---

<!-- .slide: class="alternate" -->

et on build !

---

<!-- .slide: class="alternate" -->

prêt pour la livraison continue ?

---

<!-- .slide: class="alternate" -->

c'est quoi un artefact ?

---

<!-- .slide: class="alternate" -->

et si on déployait en automatique ?

Note:
automatique en testing,
manuel en staging et production pour ne pas casser le travail des autres

<!--h-->

<!-- .slide: class="title" -->

# Monitoring d'application web

---

# À quoi ça sert ?

Monitorer, ça sert à :

* détecter les anomalies sur des environnements non maîtrisés
* avoir des informations précises sur le contexte d'apparition de l'anomalie
* tracer les anomalies dans le temps
* corréler une anomalie à un commit / une livraison

Au delà de l'applicatif, ça sert aussi à :

* avoir l'état de santé des serveurs
* détecter une surcharge et agir en prévention

---

# Les outils existants

* [New Relic](http://newrelic.com/)
* [Opbeat](http://opbeat.com/)
* [Datadog](https://app.datadoghq.com/)
* [App Dynamics](https://www.appdynamics.com/)
* [Site 24x7](https://www.site24x7.com/)
* [Sentry](http://sentry.io/)
* [SessionStack](https://www.sessionstack.com/)
* [Page Speed](https://developers.google.com/speed/pagespeed/insights/)

Zabbix

---

<!-- .slide: class="title alternate" -->

# Mise en oeuvre du monitoring avec Sentry

<!--h-->

C'est la fin...

Snif.

J'espère que ça vous a plu !

* [Revenir au sommaire](/slides/README.md#/2)
