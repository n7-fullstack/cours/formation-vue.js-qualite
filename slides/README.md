---
title: Formation Makina Sapiens - Vue.js / Qualité
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---

<!-- .slide: class="title logo" -->

# Formation Makina Sapiens Vue.js

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus

Dernière révision : Octobre 2022

---

# Organisation de la formation

* quelques règles de vie
  * horaires début / fin
  * pause repas
  * pause bio
  * téléphone sur silencieux !
* administratif (digiforma)
  * pointage des présences (pas de croix, de V ou d'initiales)
  * questionnaire à chaud (dernier jour)
  * attestations en fin de formation envoyée par mail
* autre ?

---

# Présentations...?

## Ou un petit jeu ?

https://airboardgame.net/

---

# Vous

---

# Moi

<!--h-->

# Ce que l'on va réaliser

Des... nonogrammes !

[Démo](http://n7-nonogramme-vuejs.surge.sh/)

Ou ? Une webapp carto avec Leaflet !



Avec un navigateur, un IDE, un peu de code et le framework Vue.js.

---

# Comment on va faire ?

* Vue.js
  * on va manipuler les éléments Virtuels du DOM
  * on va utiliser et user l'API de Vue.js pour nous simplifier le travail
  * on va découvrir et comprendre une architecture moderne d'applications JavaScript
* Qualité
  * on va écrire des tests unitaires à l'aide de Jest et de vue-test-utils
  * on va créer un Storybook de notre application (avec Histoire)
  * on va utiliser l'infrastructure GitLab pour déployer notre application sur Internet

---

# Comment j'interviens ?

* **j'explique les théories / concepts à connaître / comprendre**
* **j'illustre avec des exemples, des démos**
* **je peux m'arrêter à tout moment**
  * quand **vous** en avez besoin
  * quand **vous** ne comprenez pas
  * quand **vous** avez une question
  * => **vous m'arrêtez** et on prend le temps d'expliquer / répondre
* **je relis votre code**
  * vous devez le publier sur un repo gitlab public et me donner l'accès
  * vous publierez également votre tp sur surge.sh

---

# Comment vous intervenez ?

* **vous corrigez les TPs**
  * à tour de rôle, vous partagez votre écran
  * vous montrez ce que vous avez fait
  * et vous essayez de l'expliquer
  * si vous n'avez pas réussi, ou si vous avez des difficultés, on corrige en co-construction
  * si vous ne souhaitez pas participer à cette section, dites le moi

---

# Et si vous êtes perdus ?

**la solution est disponible :-)**

Vous pourrez vous recâbler à une étape de la solution si besoin est.

*Jetez vous à l'eau, j'ai la serviette pour vous essuyer !*

Abdelmalek Benzekri, IRIT Toulouse

<!--h-->

<!-- .slide: class="programme" -->

# Programme Vue.js

Les titres sont cliquables !

* [1 - Vue.js Vanilla](/slides/1_Vuejs_vanilla.md)
* [2 - Un peu d'histoire](/slides/2_Vuejs_histoire.md)
* [3 - La CLI de Vue.js et l'approche composant](/slides/3_Vuejs_cli.md)
* [4 - Le routage front](/slides/4_Vuejs_router.md)
* [5 - Vue.js avancé](/slides/5_Vuejs_avancé.md)

---

<!-- .slide: class="programme" -->

# Programme Qualité

* [6 - Qu'est ce qu'un storybook ?](/slides/6_Qualité_storybook.md)
* [7 - Écrire des tests](/slides/7_Qualité_TU.md)
* [8 - Intégration continue, déploiement et monitoring](/slides/8_Qualité_CICD.md)
