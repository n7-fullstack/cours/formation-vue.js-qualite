---
title: 3 - La CLI de Vue.js et l'approche composant
separator: <!--h-->
verticalSeparator: \n---\n
highlightTheme: monokai
css: '../makina-theme/makina-formation.css' # Here
revealOptions:
  transition: 'slide'
  slideNumber: true
  fragmentInURL: true
---


<!-- .slide: class="title logo" -->

# Formation Makina Sapiens Vue.js

## 3 - La CLI de Vue.js et l'approche composant

**Mathieu DARTIGUES**

Développeur FullStack @MakinaCorpus


---

# Objectifs La CLI de Vue.js et l'approche composant

* Savoir utiliser la CLI
* Savoir découper une application en composants (architecture)
* Savoir manipuler l'API d'un composant
  * props
  * events
  * methods
  * watch
  * computed
* Savoir étendre des composants

<!--h-->

# La CLI (4.x)

https://cli.vuejs.org/

## Déprécié !

* utilise [webpack](https://webpack.js.org/) 4 et vue-loader 15
* permet de bootstraper un projet Vue.js facilement
* adapté pour tout ce qui demandera un build system
* propose un menu facilitant les options du projet
* dispose d'un écosystème de plugins de plus en plus riche

[Documentation](https://cli.vuejs.org/)

[Projet GitHub](https://github.com/vuejs/vue-cli)

---

# npm init vue@3

* utilise [vite](https://vitejs.dev) under the hood
* et [`create-vue`](https://github.com/vuejs/create-vue)
* reprend la mécanique de la CLI avec des choix à l'installation
* bien préciser la version de vue (`vue@3` ou `vue@2`) pour éviter que npm utilise une version cachée (au sens cache disque) et donc potentiellement outdated
* contrairement à la CLI, il s'agit d'un outil de scaffolding
  * la configuration produite sera celle donnée à vite
  * et cela permet de bénéficier de tous les plugins de vite !

---

<!-- .slide: class="alternate" -->

# [Picross/Leaflet] Étape 1

instancier un projet avec `create-vue`

```bash
npm init vue@3
```

![options d'initialisation pour le projet](./assets/create-vue-params.png)

* démarrer le projet via `npm run dev`
* tester également le build avec `npm run build`

<!--h-->

<!-- .slide: class="title" -->

# Structurer son projet

---

```
|- dist                 Build de l'application
|- node_modules         Modules npm
|- public               Assets, index.html, et autres
|- src                  Code source
    |- assets            Assets à intégrer dans l'application (dont styles)
    |- components        Composants simples + stateful
       |- __tests__      Tests des composants (à juxtaposer là où l'on teste)
    |- [router]          Routes de l'application
    |- [services]        Services s'interfaçant à une API par ex.
    |- [styles]          Styles (ou intégré dans le composant)
    |- views             Layouts de l'application
|- vite.config.js        Configuration vite
```

---

* il peut ne pas y avoir de routes (si notre application n'a pas besoin d'url propres)
* **les styles** peuvent être placés :
    * dans le composant correspondant (Single File Component, en `scoped`)
    * dans un dossier comprenant tous les styles à la racine de `src`
    * dans le dossiers `src/assets`

---

<!-- .slide: class="alternate" -->

# [Picross/Leaflet] Étape 2 : reprise du code

**Consignes**

* Reprendre le code vanilla
* l'intégrer au projet

<!--h-->

<!-- .slide: class="title" -->

# L'approche composants

---

![](assets/vue-component.png)

---

# Les composants dans Vue.js

* **Single File Component** (.vue)
  * template
  * script
  * style
* Possibilité d'utiliser des langages particuliers
  * pug pour le templating
  * babel, typescript, flow, ... pour le script
  * sass, less, postcss, stylus pour le style
* **Scoped Styles**
* **Stateless components** / Dumb components
* **Statefull components** / Smart components / Views (cf vuex / pinia)
* un nom de **balise en deux mots minimum**, pour éviter des conflits html (`address` par ex)

---

# props-in / events-out

Un composant est une **boîte noire**.

On ne sait pas comment il fonctionne, 
mais il a des inputs (les propriétés qu'il déclare)
et des outputs (des événements qu'il émet).

Pour lui transmettre des propriétés,
il doit les déclarer en [`props`](https://vuejs.org/v2/api/#props)
et le parent lui fournit les valeurs via `v-bind`.

Pour capturer les événements,
il doit d'abord les émettre avec [`$emit`](https://vuejs.org/v2/api/#vm-emit)
et le parent les capture avec `v-on`.

---

# Définition d'un composant

*src/components/MonComposant.vue*

```vue
<template>
  <div>
    {{ maDataLocale }} {{ maPropriete }}
    <button @click="$emit('alert')">
      Alerte !
    </button>
  </div>
</template>

<script>
export default {
  props: {
    maPropriete: String,
  },
  emits: ['alert'],
  data () {
    maDataLocale: 'Hello',
  },
}
</script>
```

---

# Usage du composant avec v-bind et v-on

*src/App.vue*

```vue
<template>
  <MonComposant
    :maPropriete="'World !'"
    @alert="alert('Hello world !')"
  />
</template>

<script>
import MonComposant from '@/components/MonComposant.vue'

export default {
  components: {
    MonComposant,
  },
}
</script>
```

---

# Les événements en Vue.js

* directive [`v-on`](https://vuejs.org/guide/essentials/event-handling.html#listening-to-events)
  * préciser l'événement écouté : `v-on:click` ou `@click`
  * renseigner la fonction à exécuter : `v-on:click="maFonctionADeclencher"`
  * la fonction appelée prend en paramètre l'`event` qui l'a déclenchée
* [`$emit()`](https://vuejs.org/guide/components/events.html#emitting-and-listening-to-events)
  * permet de propager un événement
  * et peut également préciser des paramètres d'exécution de l'événement
  * ils seront fournis à la fonction déclenchée lors de l'événement (`v-on`)
* [`emits`](https://vuejs.org/guide/components/events.html#declaring-emitted-events)
  * permet de déclarer les événements émis par le composant
  * mais aussi de définir des fonctions de validations des événements sortant

---

# [Picross] Le composant PicrossEditor

Nous allons créer un composant `PicrossEditor` dont le rôle est :

* d'afficher une grille dont la taille est définie par le parent
* d'émettre un événement `update-picross-data` chaque fois que l'utilisateur 
modifie les données du nonogramme

Contrat :
* props: `numberRows` et `numberCells`
* events: `update-picross-data`

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 3 : Création du composant PicrossEditor

**Consignes**

* créer un composant PicrossEditor
  * qui reçoit deux propriétés `numberRows` et `numberCells`
  * qui est capable de générer de manière autonome sa structure de donnée pour mémoriser l'état d'activation de ses cellules
  * et qui émet un événement `update-picross-data`
    * chaque fois que la donnée du picross évolue (celle avec instructions)
    * pour informer le composant parent de l'évolution de cette donnée
* mettre en conformité le composant `Home` pour qu'il transmette les `numberRows` et `numberCells` que lors du clic sur le bouton de création du nonogramme

**Prérequis**

* savoir créer un Single File Component
* savoir l'importer et l'utiliser dans un template
* savoir émettre un événement depuis une instance de composant
* savoir détecter un changement d'une donnée de modèle pour déclencher un traitement
* connaître les limites du système de réactivité de Vue.js

---

# [Leaflet] Le composant AddressItem

Nous allons créer un composant `AddressItem` dont le rôle est :

* d'afficher une adresse dont les données sont fournies par le parent
* d'émettre un événement `update` chaque fois que l'utilisateur sélectionne l'adresse pour édition
Contrat
* props: `address`
* events: `update`

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 3 : Création du composant AddressItem

**Consignes**

* créer un composant AddressItem
  * qui reçoit une propriété `address`
  * qui émet un événement `update`
    * chaque fois que l'utilisateur clique sur le bouton d'édition
    * pour informer le composant parent du choix de l'utilisateur

**Prérequis**

* savoir créer un Single File Component
* savoir l'importer et l'utiliser dans un template
* savoir émettre un événement depuis une instance de composant

**Questions**

* quelle(s) directive(s) Vue.js utiliser ?


---

<!-- .slide: class="alternate" -->

# [Picross] Étape 4 : Présentation des nonogrammes

**Consignes**

* présenter au joueur l'ensemble des nonogrammes à sa disposition
  * charger le fichier `picross.json` précédemment créé
  * afficher une liste d'élément qui présente les nonogrammes
* déclencher le chargement lorsque le composant est disponible dans le DOM physique

**Prérequis**

* connaître `fetch`
* connaître `async` / `await` et/ou `Promise.then` / `Promise.catch`

---

# Le composant PicrossPlayer

Nous allons créer un composant `PicrossPlayer` dont le rôle est :

* d'afficher une grille d'un picross fournit par le parent
* avec ces indices de remplissage
* d'émettre un événement `victory` en cas de victoire du joueur

Contrat :
* props: `picrossData`
* events: `victory`

---

<!-- .slide: class="alternate" -->

# [Picross] Étape 5 : Chargement d'un nonogramme

**Consignes**

* permettre au joueur de cliquer sur un des picross,
* et initialiser le tableau avec la grille **et les indices de remplissage**
* au sein d'un nouveau composant, `PicrossPlayer`
* qui hérite du composant `PicrossEditor`
* via une prop qui s'appelle `picrossData`
* corriger les fonctions liées à la taille du nonogramme

**Prérequis**

* utiliser la destructuration de tableaux
* compréhension des methods
* connaître les limites du système de réactivité de Vue.js
* savoir étendre un composant Vue.js


---

<!-- .slide: class="alternate" -->

# [Picross] Étape 6 : Détection de la victoire

**Consignes**

* détecter quand le joueur a gagné
* utilisation d'une comparaison "totale" (comparaison des matrices des tables)
* émettre l'événement `victory` au parent
* informer le joueur qu'il a gagné

**Prérequis**

* utiliser la destructuration
* comparaison de valeurs de tableau
* manipulation des tableaux avec `join`
* connaître la mécanique des computed property
* connaître la mécanique d'un watcher
* savoir émettre des événements

---

# Le composant AddressForm

Nous allons créer un composant `AddressForm` dont le rôle est :

* de permettre la modification d'une adresse dont les données sont envoyées par le parent
* d'afficher l'ensemble des champs d'une adresse, même s'ils ne sont pas renseignés

Contrat :
* props: `address`
* events: `close`

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 4 : Création du composant AddressForm

**Consignes**

* créer un composant AddressForm
  * qui reçoit une propriété `address`
  * qui est capable de la modifier
    * soit en mutant "directement" la propriété `address` (déconseillé)
    * soit avec un `v-model` multiple
    * soit avec un `v-model` unique
  * qui émet un événement `close` lors du clic sur le bouton fermer

**Prérequis**

* savoir créer un Single File Component
* savoir l'importer et l'utiliser dans un template
* connaître la gestion d'une propriété en lecture seule
* connaître la gestion de la réactivité côté objet avec les références
* [connaître l'usage de la directive multiple `v-model`](https://thisdevbrain.com/how-to-use-v-model-in-vue-3/)

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 5 : Récupération des données d'adresse

Grâce à l'API open data

https://api-adresse.data.gouv.fr/search/?q=

**Consignes**

* ajouter un bouton au niveau du composant `AddressItem` pour déclencher une recherche d'adresse
* écrire une fonction `searchAddress` au niveau du composant `App.vue` qui appelera l'URL pour l'adresse sélectionnée
* écrire un composant `AddressResult` qui se chargera d'afficher les résultats de la recherche s'ils existent

**Prérequis**

* savoir créer un Single File Component
* savoir l'importer et l'utiliser dans un template
* savoir déclencher / intercepter un événement
* connaître la fonction `fetch`

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 6 : Afficher une carte leaflet

En utilisant la [bibliothèque Leaflet](https://leafletjs.com/)

**Consignes**

* créer un nouveau composant pour afficher la carte Leaflet avec le fond de carte
  * `https://tile.openstreetmap.org/{z}/{x}/{y}.png`
* rendre le composant réactif à partir des résultats
  * lorsque l'utilisateur fait une recherche d'adresse, afficher un marqueur pour chaque résultat sur la carte
  * lorsque la carte reçoit ces points, centrer la carte sur le premier résultat
  * lorsque l'utilisateur sélectionne un résultat, centrer la carte

**Prérequis**

* savoir créer un Single File Component
* savoir l'importer et l'utiliser dans un template
* savoir déclencher / intercepter un événement
* savoir piloter une carte via des `markers`
* savoir installer une dépendance avec npm
* connaître la bibliothèque `leafletjs`
* connaître la mécanique des watch

---

<!-- .slide: class="alternate" -->

# [Leaflet] Étape 6 bonus : Ajouter des données overpass

En utilisant la [bibliothèque Leaflet](https://leafletjs.com/)

**Consignes**

* interroger le serveur overpass pour récupérer des données des fleuves par ex.
  * à partir d'overpass : [http://wiki.openstreetmap.org/wiki/Overpass_API](http://wiki.openstreetmap.org/wiki/Overpass_API)
* Exemple d'URL
```
http://overpass-api.de/api/interpreter?data=[out:xml];(way[%22leisure%22=%22park%22](43.54506428956427,1.3108062744140625,43.663525432098275,1.571388244628906););out%20body;%3E;out%20skel%20qt;
```
* afficher dans la console **la liste des éléments** récupérés avec leur coordonnées géographiques
* afficher les résultats de la requête overpass sous forme de geojson
* réagir au déplacement de la bounding box (event `zoomend` & `moveend`)
* lors du changement de la bbox, réagir et recharger les données (attention au rate limit !)

**Prérequis**

* savoir afficher du GeoJSON

---

<!-- .slide: class="alternate" -->

```js
import osmtogeojson from "osmtogeojson";

const OVERPASS_URL = "http://overpass-api.de/api/interpreter?";
const DEFAULT_BBOX =
  "43.54506428956427,1.3108062744140625,43.663525432098275,1.571388244628906";

function getQuery(bbox = DEFAULT_BBOX) {
  let bboxString = "";
  if (bbox === DEFAULT_BBOX) {
    bboxString = DEFAULT_BBOX;
  } else {
    bboxString = `
      ${bbox.getSouth()},${bbox.getWest()},
      ${bbox.getNorth()},${bbox.getEast()}
    `;
  }

  return `data=[out:xml];(way[%22leisure%22=%22park%22](${bboxString}););out%20body;%3E;out%20skel%20qt;`;
}

export async function getOverpassData(bbox) {
  const response = await fetch(OVERPASS_URL + getQuery(bbox));
  const geojsonXML = await response.text();
  return osmtogeojson(new DOMParser().parseFromString(geojsonXML, "text/xml"));
}
```

<!--h-->

* [Revenir au sommaire](/slides/README.md#/2)
* [4 - Le routage front](/slides/4_Vuejs_router.md)
